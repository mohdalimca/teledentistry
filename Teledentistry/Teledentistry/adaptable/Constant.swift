//
//  Constant.swift
//  Clustry
//
//  Created by cis on 06/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

enum AppUser:String, Codable {
    case doctor = "2"
    case patient = "3"
}

enum Device:String {
    case iphone
    case ipad
}

enum PushTypeInteger: Int {
    case event = 1
    case activity = 2
    case cluster = 3
    case task = 4
    case list = 5
    case post = 6
}

enum PushTypeString: String {
    case event = "1"
    case activity = "2"
    case cluster = "3"
    case task = "4"
    case list = "5"
    case post = "6"
}

let multiPartImageKey = "selectedImage"
let multiPartImageNameKey = "imageKeyName"
let multiPartFileName = "fileName"


var iOSDevice: Device = .iphone
let device_type = "ios"
let device_id = UIDevice.current.identifierForVendor?.uuidString
let errorMessage = "Something went wrong"

struct APIURL { 
    static let createCluster = "groups/add-group"
    static let createCategory = "groups/add-group-category"
    static let editCluster = "groups/edit-group"
    static let createTask = "groups/create_group_task"
    static let updateTask = "groups/update_group_task"
    static let createActivity = "groups/add_group_activity"
    static let updateActivity = "groups/add_group_activity"
    static let createEvent = "groups/add_group_event"
    static let updateEvent = "groups/update_group_event"
    static let recurringType = "groups/recurringtype"
    static let updateProfile = "users/updateprofile"
    static let updateProfileImage = "users/update-profile-image"
    static let addEditTaskUpdates = "groups/add_group_task_updates"
    static let createShopping = "groups/shopping_list"
    static let addShoppingItem = "groups/add_list_items"
    static let uploadValuableDoc = "groups/upload_valuable_doc"
    static let createContact = "groups/create_important_contacts"
    static let createPost = "groups/addupdatepost"
    static let editPost = "groups/addupdatepost"
    static let updateShoppingStatus = "groups/update_Status"
}

func getJsonString(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}
