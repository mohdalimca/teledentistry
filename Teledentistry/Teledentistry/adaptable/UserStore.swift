//
//  UserStore.swift
//  Clustry
//
//  Created by cis on 14/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

struct UserStore {
    private static let token_key = "token"
    private static let fcmtoken_key = "fcmtoken"
    private static let idkey = "api_id_key"
    private static let appKey = "api_app_key"
    private static let pushTokenKey = "push_token"
    private static let userPasswordKey = "user_password"
    
    private static let fullOnboardedKey = "full_oboarded"
    private static let userIdKey = "user_id"
    private static let emailKey = "email_id"
    private static let userNameKey = "user_name"
    private static let userImageKey = "user_image"
    private static let sortLeaderBoardKey = "sort_leader_board"
    static let firstLaunchKey = "first_launch_key"
    static let firstClusterKey = "first_cluster_key"
    private static let originalPasswordKey = "original_password_key"
    private static let reminderKey = "reminder_key"
    
    
    static var token: String? {
        return UserDefaults.standard.string(forKey: token_key)
    }
    
    static var fcmtoken: String? {
        return UserDefaults.standard.string(forKey: fcmtoken_key)
    }
    
    static var password: String? {
        return UserDefaults.standard.string(forKey: originalPasswordKey)
    }
    
    static var firstLaunch: String? {
        return UserDefaults.standard.string(forKey: firstLaunchKey)
    }
    
    static var firstCluster: String? {
        return UserDefaults.standard.string(forKey: firstClusterKey)
    }
    
    static var app:String? {
        return UserDefaults.standard.string(forKey: appKey)
    }
    
    static var id:String? {
        return  UserDefaults.standard.string(forKey: idkey)
    }
    
    static var username:String? {
        return  UserDefaults.standard.string(forKey: userNameKey)
    }
    
    static var userPassword:String? {
        return UserDefaults.standard.string(forKey: userPasswordKey)
    }
    
    static var pushToken:String? {
        return UserDefaults.standard.string(forKey: pushTokenKey)
    }
    
    static var userID:String? {
        return UserDefaults.standard.string(forKey: userIdKey)
    }
    
    static var email:String? {
        return UserDefaults.standard.string(forKey: emailKey)
    }
    
    static var userImage:String? {
        return UserDefaults.standard.string(forKey: userImageKey)
    }
    
    static var fullOnboarded:Bool? {
        return UserDefaults.standard.bool(forKey: fullOnboardedKey)
    }
    
    static var sortLeaderBoard: String? {
        return UserDefaults.standard.string(forKey: sortLeaderBoardKey)
    }
    
//    static var reminder: [LocalReminder] {
//        if let data = UserDefaults.standard.value(forKey:reminderKey) as? Data {
//            let reminder = try? PropertyListDecoder().decode(Array<LocalReminder>.self, from: data)
//            return reminder ?? [LocalReminder]()
//        } else {
//            return [LocalReminder]()
//        }
//    }
    
    static func save(firstLaunch:Bool) {
        UserDefaults.standard.set(firstLaunch, forKey: firstLaunchKey)
    }
    
    static func save(firstCluster:Bool) {
        UserDefaults.standard.set(firstCluster, forKey: firstClusterKey)
    }
    
    static func save(token:String) {
        UserDefaults.standard.set(token, forKey: token_key)
    }
    
    static func save(fcmtoken:String) {
        UserDefaults.standard.set(token, forKey: fcmtoken_key)
    }
    
    static func save(password:String) {
        UserDefaults.standard.set(token, forKey: originalPasswordKey)
    }
    
    static func save(id:String) {
        UserDefaults.standard.set(id, forKey:idkey)
    }
    
    static func save(username:String) {
        UserDefaults.standard.set(username, forKey:userNameKey)
    }
    
    static func save(userImage:String) {
        UserDefaults.standard.set(userImage, forKey:userImageKey)
    }
    
    static func save(pushToken:String) {
        UserDefaults.standard.set(pushToken, forKey: pushTokenKey)
    }
    
    static func save(fullOnboarded:Bool) {
        UserDefaults.standard.set(fullOnboarded, forKey: fullOnboardedKey)
    }
    
    static func save(userID:String) {
        UserDefaults.standard.set(userID, forKey: userIdKey)
    }
    
    static func save(userPassword:String) {
        UserDefaults.standard.set(userPassword, forKey: userPasswordKey)
    }
    
    static func save(email:String) {
        UserDefaults.standard.set(email, forKey: emailKey)
    }
    
//    static func save(reminder:[LocalReminder]) {
//        UserDefaults.standard.set(try? PropertyListEncoder().encode(reminder), forKey:reminderKey)
//    }
}
