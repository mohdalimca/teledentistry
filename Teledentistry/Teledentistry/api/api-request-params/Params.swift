//
//  Params.swift
//  Clustry
//
//  Created by cis on 14/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

enum OnboardingParam {
    
    struct Signup:Codable {
        let type:AppUser?
        let email:String?
        let password:String?
        let full_name:String?
        let user_phone:String?
    }
    
    struct Login:Codable {
        let email:String?
        let password:String?
    }
    
    struct SocialLogin:Codable {
        let type:AppUser?
        let email:String?
        let social_id:String?
        let full_name:String?
    }
    
    struct ForgotPassword:Codable {
        let email:String?
    }
    
    struct ChangePassword:Codable {
        let old_password:String?
        let new_password:String?
    }
}

enum UserParams {
    struct UpdateProfile:Codable {
        let full_name:String?
        let weight:String?
        let gender:String?
        let height:String?
        let dob:String?
        let insurance:String?
        let policy:String?
    }
    
    struct SearchUser:Codable {
        let type:AppUser?
        let search_text:String?
        let page_no:Int?
    }
    
    struct CallLogs:Codable {
        let page_no:Int?
    }
    
    struct SubmitCallLog:Codable {
        let caller_user_id:String?
        let receiver_user_id:String?
        let dt_of_call:String?
        let time_of_call:String?
        let end_of_call:String?
        let purpose_call:String?
        let followup_needed:String?
    }
    
    struct Notification:Codable {
        let user_id:String?
        let room_name:String?
    }
}
