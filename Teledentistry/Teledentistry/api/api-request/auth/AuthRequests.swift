//
//  AuthRequests.swift
//  Clustry
//
//  Created by cis on 14/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

enum AuthRequests: RequestRepresentable {
    
    var method: HTTPMethod {
        return .post
    }

    var endpoint: String {
        switch self {
        case .signup:
            return "users/register"
        case .login:
            return "users/login"
        case .forgot:
            return "users/forgotpassword"
        case .changePassword:
            return "users/changepassword"
        case .socialLogin:
            return "users/sociallogin"
        case .termsAndCondition:
            return "static-pages/get-tnc"
        case .privacyPolicy:
            return "static-pages/get-privacy-policy"
        case .aboutUs:
            return "users/getAboutus"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case let .signup(params):
            return .body(data: encodeBody(data: params))
        case let .login(params):
            return .body(data: encodeBody(data: params))
        case let .forgot(params):
            return .body(data: encodeBody(data: params))
        case let .changePassword(params):
            return .body(data: encodeBody(data: params))
        case let .socialLogin(params):
            return .body(data: encodeBody(data: params))
        default:
            return .none
        }
    }
    
    case signup(params: OnboardingParam.Signup)
    case login(params: OnboardingParam.Login)
    case forgot(params: OnboardingParam.ForgotPassword)
    case changePassword(params: OnboardingParam.ChangePassword)
    case socialLogin(params: OnboardingParam.SocialLogin)
    case termsAndCondition
    case privacyPolicy
    case aboutUs
}
