//
//  TeleDetistryRequests.swift
//  Teledentistry
//
//  Created by Apple on 19/10/20.
//

import Foundation

struct TeleDetistryRequests:RequestRepresentable {
    var updateProfile: UserParams.UpdateProfile?
    var searchUser: UserParams.SearchUser?
    var callLogs: UserParams.CallLogs?
    var submitCallLog: UserParams.SubmitCallLog?
    var notification: UserParams.Notification?
    
    
    let requestType: RequestType
    enum RequestType {
        case updateProfile
        case searchUser
        case callLogs
        case submitCallLog
        case notification
    }
    
    init(requestType: RequestType) {
        self.requestType = requestType
    }
    
    init(type: RequestType, params:Codable) {
        self.requestType = type
        switch params {
        case is UserParams.UpdateProfile:
            self.updateProfile = params as? UserParams.UpdateProfile
        case is UserParams.SearchUser:
            self.searchUser = params as? UserParams.SearchUser
        case is UserParams.CallLogs:
            self.callLogs = params as? UserParams.CallLogs
        case is UserParams.SubmitCallLog:
            self.submitCallLog = params as? UserParams.SubmitCallLog
        case is UserParams.Notification:
            self.notification = params as? UserParams.Notification
        default:break
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var endpoint: String {
        switch self.requestType {
        case .updateProfile:
            return "profile_update"
        case .searchUser:
            return "groups/get-groups-by-user/\(UserStore.userID!)"
        case .callLogs:
            return "groups/add-group"
        case .submitCallLog:
            return "groups/groupDetail"
        case .notification:
            return "groups/edit-group"
        }
    }
    
    var parameters: Parameters {
        switch self.requestType {
        case .updateProfile:
            return .body(data: encodeBody(data: updateProfile))
        case .searchUser:
            return .body(data: encodeBody(data: searchUser))
        case .callLogs:
            return .body(data: encodeBody(data: callLogs))
        case .submitCallLog:
            return .body(data: encodeBody(data: submitCallLog))
        case .notification:
            return .body(data: encodeBody(data: notification))
        default:
            return .none
        }
    }
}


