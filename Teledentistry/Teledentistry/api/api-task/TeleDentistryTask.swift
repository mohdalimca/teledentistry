//
//  TeleDentistryTask.swift
//  Teledentistry
//
//  Created by Apple on 19/10/20.
//

import Foundation

final class TeleDentistryTask {
    private let dispatcher = SessionDispatcher()
    
    // MARK:- Cluster
//    func list<T:Codable>(modeling:T.Type, completion: @escaping APIResult<T>) {
//        dispatcher.execute(requst: ClusterRequests(requestType: .list), modeling: modeling, completion: completion)
//    }
//    
//    func create(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        guard let params = item as? ClusterCreatable else { return }
//        if params.image != nil {
//            dispatcher.executeMultiPart(requst: ClusterRequests(type: .create, params: item), imageArrayData: [params.image ?? Data()], imageArrayName: ["image"], attachmentName: nil, modeling: SuccessResponseModel.self, completion: completion)
//            
//        } else {
//            dispatcher.execute(requst: ClusterRequests(type: .create, params: params), modeling: SuccessResponseModel.self, completion: completion)
//        }
//    }
//    
//    func updateUserLocation(params:Codable, completion: @escaping APIResult<SuccessResponseModel>)  {
//        dispatcher.execute(requst: ClusterRequests(type: .updateUserLocation, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func deleteCluster(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .deleteCluster, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func notificationCount(completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(requestType: .notificationCount), modeling: SuccessResponseModel.self, completion: completion)
//    }
//
//    
//    func detail(params:ClusterParams.Detail, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .detail, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func edit(params:ClusterUpdatable, completion: @escaping APIResult<SuccessResponseModel>) {
//        //        if params.image != nil {
//        //            dispatcher.executeMultiPart(requst: ClusterRequests(type: .edit, params: params), imageArrayData: [params.image ?? Data()], imageArrayName: ["image"], attachmentName: nil, modeling: SuccessResponseModel.self, completion: completion)
//        //        } else {
//        //            dispatcher.execute(requst: ClusterRequests(type: .edit, params: params), modeling: SuccessResponseModel.self, completion: completion)
//        //        }
//    }
//    
//    func leave(params:ClusterParams.Detail, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .leave, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func removeUser(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .removeUser, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func updateRole(params:ClusterParams.UpdateUser, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .updateRole, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Timeline
//    func timelineFeeds(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .timelineFeeds, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func addComment(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .addComment, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func updateComment(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .updateComment, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func removeComment(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .removeComment, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    
//    func likeFeed(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .likeFeed, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func likeFeedList(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .likeFeedList, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func likeCommentList(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .likeCommentList, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func addToNoticeBoard(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .addToNoticeBoard, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func removeFeed(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .removeFeed, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func removeFromNoticeBoard(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .removeNoticeBoard, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func getNoticeBoard(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .getNoticeBoard, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Task
//    
//    func taskType(completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(requestType: .taskType), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func recurringTypes(completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(requestType: .recurringType), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func createTask(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .createTask, params: item), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func taskDetail(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .taskDetail, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func updateTask(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .updateTask, params: item), modeling:SuccessResponseModel.self , completion: completion)
//    }
//    
//    func taskList(param:Codable, type:TaskListType, completion: @escaping APIResult<SuccessResponseModel>) {
//        var request: RequestRepresentable?
//        switch type {
//        case .all:
//            request = ClusterRequests(type: .allTask, params: param)
//        case .my:
//            request = ClusterRequests(type: .myTask, params: param)
//        case .assign:
//            request = ClusterRequests(type: .assignedTask, params: param)
//        }
//        dispatcher.execute(requst: request!, modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func copyTask(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .copyTask, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func members(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .clusterMembers, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func inviteUser(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .inviteUser, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func updateTasksList(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .updateTaskList, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func addTaskUpdate(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .addTaskUpdate, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func editTaskUpdate(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .editTaskUpdate, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Activity
//    
//    func activityType(completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(requestType: .activityType), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func createActivity(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .createActivity, params: item), modeling:SuccessResponseModel.self , completion: completion)
//    }
//    
//    func activityDetail(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .activityDetail, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func copyActivity(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .copyActivity, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    
//    func memberLocations(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .memberLocation, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func updateActivity(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .updateActivity, params: item), modeling:SuccessResponseModel.self , completion: completion)
//    }
//    
//    func likeDislike(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .activityLikeDislike, params: item), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func likeDislikeList(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .likeDislikeList, params: item), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func activityList(param:Codable, type:ActivityListType, completion: @escaping APIResult<SuccessResponseModel>) {
//        var request: RequestRepresentable?
//        switch type {
//        case .my:
//            request = ClusterRequests(type: .myActivity, params: param)
//        case .cluster:
//            request = ClusterRequests(type: .clusterActivity, params: param)
//        case .history:
//            request = ClusterRequests(type: .historyActivity, params: param)
//        }
//        dispatcher.execute(requst: request!, modeling: SuccessResponseModel.self, completion: completion)
//    }
//    func clusterActivity<T:Codable>(page:Int, id:String, modeling:T.Type, completion: @escaping APIResult<T>) {
//        //        dispatcher.execute(requst: ClusterRequests.init(type: .clusterActivity(page: page), params: ActivityParams.List(group_id: id)), modeling: modeling, completion: completion)
//    }
//    
//    // MARK:- Leaderboard
//    func leaderboard(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .leaderboard, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func addExtraRewardPoint(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .addExtraRewardPoint, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    
//    
//    
//    // MARK:- Contacts
//    func contacts(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .contacts, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func createContact(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .createContact, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func updateContact(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .updateContact, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func deleteContact(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .deleteContact, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    
//    // MARK:- Events
//    
//    func eventsDesignation(completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(requestType: .eventDesignation), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func createEvent(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .createEvent, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func eventsList(param:Codable, type:EventsListType, completion: @escaping APIResult<SuccessResponseModel>) { var request: RequestRepresentable?
//        switch type {
//        case .my:
//            request = ClusterRequests(type: .myEvents, params: param)
//        case .cluster:
//            request = ClusterRequests(type: .clusterEvents, params: param)
//        case .history:
//            request = ClusterRequests(type: .historyEvents, params: param)
//        }
//        dispatcher.execute(requst: request!, modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func eventDetail(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .eventDetail, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func updateEvent(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .updateEvent, params: item), modeling:SuccessResponseModel.self , completion: completion)
//    }
//    
//    func addCustomDesignation(item:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .addCustomDesignation, params: item), modeling:SuccessResponseModel.self , completion: completion)
//    }
//    
//    func copyEvent(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .copyEvent, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Countdown
//    func countDownList(param:Codable, type:CountdownListType, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .countDown, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Valuable Docs and Cluster Media
//    func valuableDocs(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .valuableDocs, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func clusterAttachments(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .clusterMedia, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func updateBills(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .updateBills, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func deleteValuableDocs(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .deleteValuableDoc, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Shopping List
//    func createList(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .createList, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Search User
//    func search(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .search, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Notifications
//    func notifications(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .notifications, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func acceptClusterRequest(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .acceptClusterRequest, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func declineClusterRequest(param:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .declineClusterRequest, params: param), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    func notificationSeen(completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(requestType: .notificationSeen), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Post
//    func postDetail(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .postDetail, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
//    
//    // MARK:- Dashboard
//    func dashboard(params:Codable, completion: @escaping APIResult<SuccessResponseModel>) {
//        dispatcher.execute(requst: ClusterRequests(type: .dashboard, params: params), modeling: SuccessResponseModel.self, completion: completion)
//    }
}
