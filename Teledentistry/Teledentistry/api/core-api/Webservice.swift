//
//  Webservice.swift
//  Clustry
//
//  Created by cis on 21/01/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit
import Alamofire

enum AppAttachmentType: String, Codable {
    case image
    case video
    case audio
    case doc
}

class WebService {
    typealias JSONTaskCompletionHandler = (Decodable?, String?) -> Void
    
    func request<T: Decodable, U: Encodable>(urlString: String, httpMethod : HTTPMethod, parameters: U, decodingType: T.Type, completion: @escaping JSONTaskCompletionHandler ) {
        
        let encoder = JSONEncoder()
        let jsonData = try! encoder.encode(parameters)
        
        guard let url = URL(string: "\(APIEnvironment.host)/\(APIEnvironment.APIVersion)\(urlString)") else { return }
        var request = URLRequest.init(url: url)
        let headers: HTTPHeaders = [
//            "Accept": UserStore.token ?? "",
            "Content-Type": "application/json; charset=UTF-8"
        ]
        request.httpBody = jsonData
        
        let parameters: [String: Any] = [
            "IdQuiz" : 102,
            "IdUser" : "iosclient",
            "User" : "iosclient",
            "List": [
                [
                    "IdQuestion" : 5,
                    "IdProposition": 2,
                    "Time" : 32
                ],
                [
                    "IdQuestion" : 4,
                    "IdProposition": 3,
                    "Time" : 9
                ]
            ]
        ]
        
        AF.request(url, method:.post, parameters: parameters,encoding: JSONEncoding.default).responseJSON { (response) in
            
        }
        
        AF.request(url).validate().response { (response) in
            
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func requestMultiPart<T: Decodable, U: Encodable>(urlString: String, httpMethod : HTTPMethod, parameters: U, decodingType: T.Type, imageArray: [[String : Any]], fileArray:[[String:Any]], completion: @escaping JSONTaskCompletionHandler ) {
        
        let jsonData = try! JSONEncoder().encode(parameters)
        let jsonString = String(data: jsonData, encoding: .utf8)
        print("jsonString: \(String(describing: jsonString))")
        
        let s = "\(APIEnvironment.host)/\(APIEnvironment.APIVersion)\(urlString)"
        print("Requesting url == \(s)")
        let scaped = s.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: scaped!)
        let headers: HTTPHeaders = [
            "Authorization": UserStore.token ?? "",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        // Use Alamofire to upload the image
        let dict : [String : Any] = convertToDictionary(text: jsonString!)!
        print(dict)
        
        AF.upload(multipartFormData: { multipartFormData in
            for (key, value) in dict {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            for dict in imageArray {
                if let image = (dict[multiPartImageKey] as? UIImage) {
                    let imageData = image.compressImage!
                    multipartFormData.append(imageData, withName: "\(dict[multiPartImageNameKey] as! String)", fileName: "image.jpg", mimeType: "image/jpeg")
                }
            }
            
//            for dict in fileArray {
//                if let url = (dict[multiPartImageKey] as? URL) {
//                    do {
//                        let fileData = try Data.init(contentsOf: url, options: .alwaysMapped)
//                        print("request mime type ======= \(url.mimeType())")
//                        multipartFormData.append(fileData, withName: "\(dict[multiPartImageNameKey] as! String)".replace(string: " ", replacement: ""), fileName: "\(dict[multiPartFileName] as! String)".replace(string: " ", replacement: "_"), mimeType: url.mimeType())
//                    } catch {}
//                }
//            }
            
        },to: url!, usingThreshold: UInt64.init(),
          method: .post,
          headers: headers).response { encodingResult in
            switch encodingResult.result {
            case .success(let result):
                if let data = result {
                    do {
                        let genericModel = try JSONDecoder().decode(decodingType, from: data)
                        completion(genericModel, nil)
                    } catch let err {
                        completion(nil, err.localizedDescription)
                    }
                } else {
                    completion(nil, encodingResult.error?.localizedDescription)
                }
                
            case .failure(let error):
                print("upload err: \(error.localizedDescription)")
                completion(nil, error.localizedDescription)
            }
        }
    }
}

protocol MyEncodable: Encodable {
    func toJSONData() -> Data?
}

extension MyEncodable {
    func toJSONData() -> Data?{
        return try? JSONEncoder().encode(self)
    }
}


public extension URL {
    
    func fetchPageInfo(_ completion: @escaping ((_ title: String?, _ description: String?, _ previewImage: String?) -> Void), failure: @escaping ((_ errorMessage: String) -> Void)) {
//
//        var request = URLRequest(url: self)
//        let newUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36"
//        request.setValue(newUserAgent, forHTTPHeaderField: "User-Agent")
//        AF.request(self).validate().response { (result) in
//            if let data = result.data {
//                if let doc = try? Kanna.HTML(html: data, encoding: String.Encoding.utf8) {
//                    let title = doc.title
//                    var description: String? = nil
//                    var previewImage: String? = nil
//
//                    if let nodes = doc.head?.xpath("//meta").enumerated() {
//                        for node in nodes {
//                            if node.element["property"]?.contains("description") == true ||
//                                node.element["name"] == "description" {
//                                description = node.element["content"]
//                            }
//
//                            if node.element["property"]?.contains("image") == true &&
//                                node.element["content"]?.contains("http") == true {
//                                previewImage = node.element["content"]
//                            }
//                        }
//                    }
//                    completion(title, description, previewImage)
//                }
//            } else {
//                completion(nil, nil, nil)
//            }
//        }
    }
}

