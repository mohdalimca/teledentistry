//
//  App.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import UIKit

class App {
    private var window:UIWindow?
    private var welcomeCoordinator: WelcomeCoordinator
    
    private let router = Router()
    init() {
        let router = Router()
        welcomeCoordinator = WelcomeCoordinator(router: router)
    }
    
    func start(window:UIWindow) {
        self.window = window
        if UserStore.token != nil {
//            landingCoordinator.start(with: .home, animate: true)
//            window.rootViewController = landingCoordinator.toPresentable()
        } else {
            welcomeCoordinator.start()
            window.rootViewController = welcomeCoordinator.toPresentable()
        }
    }
}
