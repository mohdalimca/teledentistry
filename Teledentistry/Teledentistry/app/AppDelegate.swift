//
//  AppDelegate.swift
//  Teledentistry
//
//  Created by Apple on 19/10/20.
//

import UIKit
import Firebase
import GoogleSignIn
import IQKeyboardManagerSwift
import UserNotifications


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var appCoordinator = App()
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        if let window = window {
            appCoordinator.start(window: window)
        }
        UIApplication.shared.registerForRemoteNotifications()
        requestNotificationAuthorization(application: application)
        configureGoogle()
        configureIQKeyboard()
        configureMessaging()
        return true
    }
    
    private func configureIQKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 0
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    private func configureMessaging() {
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
    }
    
    private func configureGoogle() {
        GIDSignIn.sharedInstance().clientID = "com.googleusercontent.apps.1024011725737-db8jrkd89sjo9jhuc5ihh9jhsur241t2"
        GIDSignIn.sharedInstance().delegate = self
    }
    
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserStore.save(fcmtoken: fcmToken)
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func requestNotificationAuthorization(application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in }
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Devcie token for apns ==== \(token)")
        Messaging.messaging().apnsToken = deviceToken
        UserStore.save(pushToken: token)
    }
    
    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(userInfo)")
        
        if application.applicationState != .background || application.applicationState !=  .inactive  {
//            let aps = RV_CheckDataType.getDictionary(anyDict: userInfo["aps"] as AnyObject)
//            if aps.count > 0 {
//                if let alert = aps.value(forKey: "alert") as? NSDictionary
//                {
//                    let message = RV_CheckDataType.getString(anyString: alert.value(forKey: "body") as AnyObject)
//                    let title = RV_CheckDataType.getString(anyString: alert.value(forKey: "title") as AnyObject)
//                    let alert = UIAlertController(title: title, message:message, preferredStyle: UIAlertController.Style.alert)
//                    // add an action (button)
//                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//                    // show the alert
//                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//                }
//            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        NSLog("Couldn't Register \(error)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Notification received")
        if let userInfo = response.notification.request.content.userInfo as? [String : AnyObject] {
          
        }
        completionHandler()
    }
}

extension AppDelegate: GIDSignInDelegate {
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        //        let userId = user.userID                  // For client-side use only!
        //        let idToken = user.authentication.idToken // Safe to send to the server
        //        let fullName = user.profile.name
        //        let givenName = user.profile.givenName
        //        let familyName = user.profile.familyName
        //        let email = user.profile.email
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

}
