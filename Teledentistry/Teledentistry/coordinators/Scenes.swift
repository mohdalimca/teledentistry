//
//  Scenes.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

enum Scenes:String {
    case welcome
    case login
    case signup
    case forgot
    case dashboard
    case settings
    case changePassword
    case aboutus
    case feedback
    case privacy
    case terms
    case logout
    case myProfile
    case editProfile
    case datePicker
    case pickerView
    case browser
    case inputAlert
}

enum Storyboard:String {
    case main = "Main"
}

enum StoryboardIdentifier:String {
    case welcome
    case login
    case signup
    case forgot
    case dashboard
    case settings
    case changePassword
    case aboutus
    case feedback
    case privacy
    case terms
    case logout
    case myProfile
    case editProfile
    case datePicker
    case pickerView
    case browser
    case inputAlert
}
