//
//  Extensions.swift
//  Clustry
//
//  Created by cis on 06/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialSnackbar
import AVKit
import AVFoundation
import SVProgressHUD
import SDWebImage

extension UITableView {
    private struct AssociatedObjectKey {
        static var registeredCells = "registeredCells"
    }
    
    private var registeredCells: Set<String> {
        get {
            if objc_getAssociatedObject(self, &AssociatedObjectKey.registeredCells) as? Set<String> == nil {
                self.registeredCells = Set<String>()
            }
            return objc_getAssociatedObject(self, &AssociatedObjectKey.registeredCells) as! Set<String>
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedObjectKey.registeredCells, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func reloadSection(section:Int, animation: UITableView.RowAnimation = .none) {
        self.reloadSections(IndexSet(integer: section), with: animation)
    }
    
    func registerReusable<T: UITableViewCell>(_: T.Type) where T: Reusable {
        let bundle = Bundle(for: T.self)
        
        if bundle.path(forResource: T.reuseIndentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: T.reuseIndentifier, bundle: bundle)
            register(nib, forCellReuseIdentifier: T.reuseIndentifier)
        } else {
            register(T.self, forCellReuseIdentifier: T.reuseIndentifier)
        }
    }
    
    func dequeueReusable<T: UITableViewCell>(_ indexPath: IndexPath) -> T where T: Reusable {
        if registeredCells.contains(T.reuseIndentifier) == false {
            registeredCells.insert(T.reuseIndentifier)
            registerReusable(T.self)
        }
        
        guard let reuseCell = self.dequeueReusableCell(withIdentifier: T.reuseIndentifier, for: indexPath) as? T else { fatalError("Unable to dequeue cell with identifier \(T.reuseIndentifier)") }
        return reuseCell
    }
    
    func registerReusable<T: UITableViewCell>(_: T.Type) where T: ReusableReminder {
        let bundle = Bundle(for: T.self)
        
        if bundle.path(forResource: T.reuseIndentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: T.reuseIndentifier, bundle: bundle)
            register(nib, forCellReuseIdentifier: T.reuseIndentifier)
        } else {
            register(T.self, forCellReuseIdentifier: T.reuseIndentifier)
        }
    }
    
    func dequeueReusable<T: UITableViewCell>(_ indexPath: IndexPath) -> T where T: ReusableReminder {
        if registeredCells.contains(T.reuseIndentifier) == false {
            registeredCells.insert(T.reuseIndentifier)
            registerReusable(T.self)
        }
        
        guard let reuseCell = self.dequeueReusableCell(withIdentifier: T.reuseIndentifier, for: indexPath) as? T else { fatalError("Unable to dequeue cell with identifier \(T.reuseIndentifier)") }
        return reuseCell
    }
}

extension UICollectionView {
    private struct AssociatedObjectKey {
        static var registeredCells = "registeredCells"
    }
    
    private var registeredCells: Set<String> {
        get {
            if objc_getAssociatedObject(self, &AssociatedObjectKey.registeredCells) as? Set<String> == nil {
                self.registeredCells = Set<String>()
            }
            return objc_getAssociatedObject(self, &AssociatedObjectKey.registeredCells) as! Set<String>
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedObjectKey.registeredCells, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func registerReusable<T: UICollectionViewCell>(_: T.Type) where T: Reusable {
        let bundle = Bundle(for: T.self)
        
        if bundle.path(forResource: T.reuseIndentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: T.reuseIndentifier, bundle: bundle)
            register(nib, forCellWithReuseIdentifier: T.reuseIndentifier)
            
        } else {
            register(T.self, forCellWithReuseIdentifier: T.reuseIndentifier)
        }
    }
    
    func dequeueReusable<T: UICollectionViewCell>(_ indexPath: IndexPath) -> T where T: Reusable {
        if registeredCells.contains(T.reuseIndentifier) == false {
            registeredCells.insert(T.reuseIndentifier)
            registerReusable(T.self)
        }
        guard let reuseCell = self.dequeueReusableCell(withReuseIdentifier: T.reuseIndentifier, for: indexPath) as? T else {
            fatalError("Unable to dequeue cell with identifier \(T.reuseIndentifier)")
        }
        return reuseCell
    }
    
    func registerReusable<T: UICollectionViewCell>(_: T.Type) where T: ReusableReminder {
        let bundle = Bundle(for: T.self)
        
        if bundle.path(forResource: T.reuseIndentifier, ofType: "nib") != nil {
            let nib = UINib(nibName: T.reuseIndentifier, bundle: bundle)
            register(nib, forCellWithReuseIdentifier: T.reuseIndentifier)
            
        } else {
            register(T.self, forCellWithReuseIdentifier: T.reuseIndentifier)
        }
    }
    
    func dequeueReusable<T: UICollectionViewCell>(_ indexPath: IndexPath) -> T where T: ReusableReminder {
        if registeredCells.contains(T.reuseIndentifier) == false {
            registeredCells.insert(T.reuseIndentifier)
            registerReusable(T.self)
        }
        guard let reuseCell = self.dequeueReusableCell(withReuseIdentifier: T.reuseIndentifier, for: indexPath) as? T else {
            fatalError("Unable to dequeue cell with identifier \(T.reuseIndentifier)")
        }
        return reuseCell
    }
}


extension UIViewController {
    static func from<T>(storyboard: Storyboard) -> T {
        guard let controller = UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateInitialViewController() as? T else {
            fatalError("unable to instantiate view controller")
        }
        
        return controller
    }
    
    func endViewEditing() {
        self.view.endEditing(true)
    }
    
    func startAnimation() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.show()
    }
    
    func stopAnimating() {
        SVProgressHUD.dismiss()
    }
    
    func playAudioFile(_ link:String) {
        let strURL = "\(APIEnvironment.host)/\(link)"
        let videoURL = URL(string: strURL)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func playVideoFile(_ link:String) {
        let strURL = "\(APIEnvironment.host)/\(link)"
        let videoURL = URL(string: strURL)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func playAudioFromServer(_ link: String) {
        let strURL = "\(APIEnvironment.host)/\(link)"
        let audioURL = URL(string: strURL)
        let player = AVPlayer(url: audioURL!)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
        player.play()
    }
    
    
    func playLocalAudio(_ path: URL?) {
        guard let _ = path else { return }
        let player = AVPlayer(url: path!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func canOpenAttachment(_ link:String) -> (Bool) {
        if UIApplication.shared.canOpenURL(link.imageURL) {
            return true
        } else {
            return false
        }
    }


    
//    func startAnimation() {
//        startAnimating(type: .circleStrokeSpin)
//    }
    
    func showSnackBar(_ msg: String) {
        let message = MDCSnackbarMessage()
        message.text = msg
        MDCSnackbarManager.default.show(message)
    }
    
    static func from<T>(from storyboard: Storyboard, with identifier: StoryboardIdentifier) -> T {
        guard let controller = UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateViewController(withIdentifier: identifier.rawValue) as? T else {
            fatalError("unable to instantiate view controller")
        }
        return controller
    }
    
    func showPopup(_ isEmailAddress: Bool, controller: UIViewController) {
        let alert = InputFieldAlert(nibName: "InputFieldAlert", bundle: nil)
        let pop = CustomPopup.shared.showPopup(controller: alert, buttons: [])
        controller.present(pop, animated: true, completion: nil)
        alert.isEmailAddress = isEmailAddress
    }
    
    func showDatePickerOn(controller: UIViewController, scene:Scenes, pickerMode:UIDatePicker.Mode, setMinDate:Bool?, setMaxDate:Bool?) {
        let picker: DatePicker = DatePicker.from(from: .main, with: .datePicker)
        picker.setMinDate  = setMinDate
        picker.setMaxDate = setMaxDate
//        switch controller {
//        case is CreateEventViewController:
//            picker.delegate = controller as! CreateEventViewController
//        case is CreateTaskViewController:
//            picker.delegate = controller as! CreateTaskViewController
//        case is EditEventViewController:
//            picker.delegate = controller as! EditEventViewController
//        case is EditTaskViewController:
//            picker.delegate = controller as! EditTaskViewController
//        case is EditActivityViewController:
//            picker.delegate = controller as! EditActivityViewController
//        case is EditProfileViewController:
//            picker.delegate = controller as! EditProfileViewController
//        default:
//            break
//        }
        picker.selectedValue = ""
        picker.mode = pickerMode
        picker.scene = scene
        picker.modalTransitionStyle = .crossDissolve
        picker.modalPresentationStyle = .overFullScreen
        self.present(picker, animated: true, completion: nil)
    }
    
    func showInputAlert(controller: UIViewController, type:InputFieldAlertType, editValue:String = "") {
        let alert: InputFieldAlert = InputFieldAlert.from(from: .main, with: .inputAlert)
        alert.alertType = type
        alert.editValue = editValue
        
//        switch controller {
//        case is WelcomeViewController:
//            alert.delegate = controller as! WelcomeViewController
//        case is LoginViewController:
//            alert.delegate = controller as! LoginViewController
//        case is CreateActivityViewController:
//            alert.delegate = controller as! CreateActivityViewController
//        case is CreateEventViewController:
//            alert.delegate = controller as! CreateEventViewController
//        case is EditEventViewController:
//            alert.delegate = controller as! EditEventViewController
//        case is MoreViewController:
//            alert.delegate = controller as! MoreViewController
//        case is CreateTaskViewController:
//            alert.delegate = controller as! CreateTaskViewController
//        case is EditTaskViewController:
//            alert.delegate = controller as! EditTaskViewController
//        case is EditProfileViewController:
//            alert.delegate = controller as! EditProfileViewController
//        case is TaskUpdateViewController:
//            alert.delegate = controller as! TaskUpdateViewController
//        case is EditActivityViewController:
//            alert.delegate = controller as! EditActivityViewController
//        case is TimelineViewController:
//            alert.delegate = controller as! TimelineViewController
//        case is CreatePostViewController:
//            alert.delegate = controller as! CreatePostViewController
//        case is EditPostViewController:
//            alert.delegate = controller as! EditPostViewController
//        case is LeaderboardViewController:
//            alert.delegate = controller as! LeaderboardViewController
//        case is ShoppingDetailViewController:
//            alert.delegate = controller as! ShoppingDetailViewController
//        default: break
//        }
        let pop = CustomPopup.shared.showPopup(controller: alert, buttons: [])
        self.present(pop, animated: true, completion: nil)
    }
    
    func showCustomAlert(controller: UIViewController) {
        //        let alert = CustomAlert(nibName: "CustomAlert", bundle: nil)
        //        alert.delegate = (controller as! EditViewController)
        //        let pop = CustomPopup.shared.showPopup(controller: alert, buttons: [])
        //        controller.present(pop, animated: true, completion: nil)
    }
    
    func showChallengeStartUpsell(controller: UIViewController) {
        //        let alert = ChallengeStartUpsell(nibName: "ChallengeStartUpsell", bundle: nil)
        //        //        alert.delegate = (controller as! HomeViewController)
        //        alert.delegate = (controller as! HomeViewController)
        //        let pop = CustomPopup.shared.showPopup(controller: alert, buttons: [])
        //        controller.present(pop, animated: true, completion: nil)
    }
    
    func showChallengePreviewUpsell(controller: UIViewController) {
        //        let alert = ChallengePreviewUpsell(nibName: "ChallengePreviewUpsell", bundle: nil)
        //        //        alert.delegate = (controller as! HomeViewController)
        //        let pop = CustomPopup.shared.showPopup(controller: alert, buttons: [])
        //        controller.present(pop, animated: true, completion: nil)
    }
    
    func showChallengeInfoAlert(controller: UIViewController) {
        //        let alert = ChallengeInfoAlert(nibName: "ChallengeInfoAlert", bundle: nil)
        //        let pop = CustomPopup.shared.showPopup(controller: alert, buttons: [])
        //        controller.present(pop, animated: true, completion: nil)
    }
    
    func addTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(screenTapped(_:)))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func screenTapped(_ tap:UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}

extension StringProtocol {
    var firstUppercased: String {
        return prefix(1).uppercased()  + dropFirst()
    }
    var firstCapitalized: String {
        return prefix(1).capitalized + dropFirst()
    }
}

extension String {
    
    var isInteger: Bool { return Int(self) != nil }
    var isDouble: Bool { return Double(self) != nil }
    
    
    
    func float() -> CGFloat? {
      guard let doubleValue = Double(self) else {
        return nil
      }
      return CGFloat(doubleValue)
    }

    var imageURL: URL {
        let imageLink = "\(APIEnvironment.host)/\(self)"
        return URL(string: imageLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!
    }
    
    func playAudio() {
        var audioPlayer: AVAudioPlayer?
        let strURL = "\(APIEnvironment.host)/\(self)"
        if let alertSound = URL.init(string: strURL) {
            print(alertSound.absoluteString)
            try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try! AVAudioSession.sharedInstance().setActive(true)
            try! audioPlayer = AVAudioPlayer(contentsOf: alertSound)
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        }
    }
    
    var isSingleEmoji: Bool { count == 1 && containsEmoji }

    var containsEmoji: Bool { contains { $0.isEmoji } }

    var containsOnlyEmoji: Bool { !isEmpty && !contains { !$0.isEmoji } }

    var emojiString: String { emojis.map { String($0) }.reduce("", +) }

    var emojis: [Character] { filter { $0.isEmoji } }

    var emojiScalars: [UnicodeScalar] { filter { $0.isEmoji }.flatMap { $0.unicodeScalars } }
    
    func openBrowser() {
        let url = URL(string: self)
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
    }
    
    
    var userName: String {
        let name = self.components(separatedBy: " ")
        guard name.count > 0 else { return "" }
        let first = name[0]
        if name.count >= 2 {
            return "\(first)" + " " + "\(name[1].first?.uppercased() ?? "")."
        }else {
            return first
        }
    }
    
    var databaseToUnix: TimeInterval {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: self)
        return Date().timeIntervalSince(date!)
    }
    
    func listCompletedDate(format: String = "yyyy-MM-dd'T'HH:mm:sssZ") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return dateFormatter.string(from: date!)
    }
    
    func imageFromBase64() -> UIImage? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return UIImage(data: data)
    }
    
    var toFloat: CGFloat {
        var doubleValue: Double = 0
        var convertValue: Double = NumberFormatter().number(from: self)?.doubleValue ?? 0
        doubleValue = convertValue
        if convertValue != 0 {
            return CGFloat(doubleValue)
        }
        
        convertValue = Double(self) ?? 0
        doubleValue = convertValue
        if convertValue != 0 {
            return CGFloat(doubleValue)
        }
        
        convertValue = (self as NSString).doubleValue
        doubleValue = convertValue
        if convertValue != 0 {
            return CGFloat(doubleValue)
        }
        
        return CGFloat(doubleValue)
    }
    
    
    var toDouble: Double {
        var doubleValue: Double = 0
        var convertValue: Double = NumberFormatter().number(from: self)?.doubleValue ?? 0
        doubleValue = convertValue
        if convertValue != 0 {
            return doubleValue
        }
        
        convertValue = Double(self) ?? 0
        doubleValue = convertValue
        if convertValue != 0 {
            return doubleValue
        }
        
        convertValue = (self as NSString).doubleValue
        doubleValue = convertValue
        if convertValue != 0 {
            return doubleValue
        }
        
        return doubleValue
    }
    
    var toInt: Int {
        return NumberFormatter().number(from: self)?.intValue ?? 0
    }
    
    
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
    
    var challengeDate: String? {
        var date: Date?
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "MM/dd/yyyy"
        if date != nil {
            return dateFormatter.string(from: date ?? Date())
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
            date = dateFormatter.date(from: self)
            dateFormatter.dateFormat = "MM/dd/yyyy"
            return dateFormatter.string(from: date ?? Date())
        }
    }
    
    var isChallengeStarted: Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let date = dateFormatter.date(from: self)
        let order = Calendar.current.compare(date!, to: Date(), toGranularity: .day)
        switch order {
        case .orderedAscending, .orderedSame:
            return true
        case .orderedDescending:
            return false
        }
    }
    
    var daysDiffernce: Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: self)
        let component = Set<Calendar.Component>([.day, .month, .year])
        let difference = Calendar.current.dateComponents(component, from: Date(), to: date ?? Date())
        return difference.day
    }
    
    func dateFrom(format: String, in other:String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = other
        return dateFormatter.string(from: date!)
    }
    
    func insertSeparator(_ separatorString: String, atEvery n: Int) -> String {
        guard 0 < n else { return self }
        return self.enumerated().map({String($0.element) + (($0.offset != self.count - 1 && $0.offset % n ==  n - 1) ? "\(separatorString)" : "")}).joined()
    }
    
    mutating func insertedSeparator(_ separatorString: String, atEvery n: Int) {
        self = insertSeparator(separatorString, atEvery: n)
    }
    
    var date: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let toDate = dateFormatter.date(from: self)
        return toDate ?? Date()
    }
    
    func encodeEmojis() -> String {
        let data = self.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func decodeEmojis() -> String? {
        let data = self.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
}

extension TimeInterval {
    var unixToDatabase: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.string(from: date)
    }

    var unixToEventDate: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy h:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: date)
    }

    
    var dateMonth: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM"
        return dateFormatter.string(from: date)
    }
    
    var feedCreateDate: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy h:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: date)
    }
    
    var notificationDate:String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a MMM yyyy"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: date)
    }
    
//    var detailedDate: String {
//        let date = Date(timeIntervalSince1970: self)
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = ClustryDateFormate.dateAndTime.rawValue
//        return dateFormatter.string(from: date)
//    }
    
    var taskRemainingTime: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d HH:mm"
        dateFormatter.timeZone = .current
        return dateFormatter.string(from: date)
    }
    
    func daysDiffernce(end:TimeInterval) -> Int {
        let startDate = Date(timeIntervalSince1970: self)
        let endDate = Date(timeIntervalSince1970: end)
        let component = Set<Calendar.Component>([.day, .month, .year])
        let difference = Calendar.current.dateComponents(component, from: startDate, to: endDate)
        return difference.day ?? 0
    }
    
    
    var daysPassed: Int {
        let createDate = Date(timeIntervalSince1970: self)
        let component = Set<Calendar.Component>([.day, .month, .year])
        let difference = Calendar.current.dateComponents(component, from: Date(), to: createDate)
        return difference.day ?? 0
    }
    
    
    func dateFromTimestamp() -> Date {
        return Date(timeIntervalSince1970: self)
    }
}

extension Calendar {
    static let gregorian = Calendar(identifier: .gregorian)
}

extension Date {
    
    func daysDiffernce(end:Date) -> Int {
        let component = Set<Calendar.Component>([.day, .month, .year])
        let difference = Calendar.current.dateComponents(component, from: self, to: end)
        return difference.day ?? 0
    }
    
    func timeInterval(from date: Date) -> TimeInterval {
        let second = Calendar.current.dateComponents([.second], from: self, to: date).second ?? 0
        return TimeInterval(second)
    }
    
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    
    func timeintervalDiffernce(end:Date) -> TimeInterval {
        let component = Set<Calendar.Component>([.second])
        let difference = Calendar.current.dateComponents(component, from: self, to: end)
        return TimeInterval(difference.day ?? 0)
    }
    
    var startOfDay: Date {
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([.year, .month, .day])
        let components = calendar.dateComponents(unitFlags, from: self)
        return calendar.date(from: components)!
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        let date = Calendar.current.date(byAdding: components, to: self.startOfDay)
        return date!
    }
    
    var startOfWeek: Date {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
    }
    
    var endOfWeek: Date {
        var components = DateComponents()
        components.day = 7
        return Calendar.gregorian.date(byAdding: components, to: self.startOfWeek)!
    }
    
    var startOfMonth: Date {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.year, .month], from: Calendar.gregorian.startOfDay(for: self)))!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        return Calendar.gregorian.date(byAdding: components, to: self.startOfMonth)!
    }
    
    var startOfYear: Date {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.year], from: Calendar.gregorian.startOfDay(for: self)))!
    }
    
    var endOfYear: Date {
        var components = DateComponents()
        components.year = 1
        return Calendar.gregorian.date(byAdding: components, to: self.startOfYear)!
    }
    
    var currentDate: Int {
        let calendar = Calendar.current
        let compomnents = calendar.dateComponents([.day], from: self)
        return compomnents.day ?? 0
    }
    
    var currentMonth: Int {
        let calendar = Calendar.current
        let compomnents = calendar.dateComponents([.month], from: self)
        return compomnents.month ?? 0
    }
    
    var currentYear: Int {
        let calendar = Calendar.current
        let compomnents = calendar.dateComponents([.year], from: self)
        return compomnents.year ?? 0
    }
    
    var currentMonthName: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self).uppercased()
    }
    
    var calenderMonth: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM yy"
        return dateFormatter.string(from: self).capitalized
    }
    
    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: self)
    }
    
    var calendarDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE dd MMM"
        return dateFormatter.string(from: self)
    }
    
    var dateFrom1970: Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.date(from: "01/01/1970")!
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func adding(hours: Int) -> Date {
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)!
    }
    
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    func adding(weeks: Int) -> Date {
        return Calendar.current.date(byAdding: .weekday, value: weeks, to: self)!
    }
    
    func adding(months: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
    
    func adding(years: Int) -> Date {
        return Calendar.current.date(byAdding: .year, value: years, to: self)!
    }
    
    func UTCToLocal(outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = outGoingFormat
        return dateFormatter.string(from: self)
    }
    
    func getElapsedInterval() -> String {
        let interval = Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: self, to: Date())
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let week = interval.weekOfMonth, week > 0 {
            return week == 1 ? "\(week)" + " " + "week ago" :
                "\(week)" + " " + "weeks ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" :
                "\(hour)" + " " + "hours ago"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "min ago" :
                "\(minute)" + " " + "min ago"
        } else {
            return "a moment ago"
        }
    }
    
    func journalCountFormattedDate(format: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func journalHeaderFormattedDate(format: String = "E , MMM d YYYY") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func isSameAsToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    func getDaysInMonth() -> Int{
        let calendar = Calendar.current
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        return numDays
    }
    
    func getRangeForCurrentMonth() -> String {
        var range = ""
        var date =  Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        range.append("\(formatter.string(from: date)) ⏤ ")
        date = Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: date)!
        range.append(formatter.string(from: date))
        return range.uppercased()
    }
    
    func getRangeForCurrentWeek() -> String {
        var range = ""
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return ""  }
        //        var date = gregorian.date(byAdding: .day, value: 1, to: sunday)
        var date = sunday
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        range.append("\(formatter.string(from: date)) ⏤ ")
        date = gregorian.date(byAdding: .day, value: 7, to: sunday)!
        range.append(formatter.string(from: date))
        return range.uppercased()
    }
    
    func getSummaryRangeForWeek() -> String {
        var range = ""
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return ""  }
        //        var date = gregorian.date(byAdding: .day, value: 1, to: sunday)
        var date = sunday
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d"
        range.append("\(formatter.string(from: date)) ⏤ ")
        date = gregorian.date(byAdding: .day, value: 7, to: sunday)!
        range.append(formatter.string(from: date))
        range.append(", \(currentYear)")
        return range.uppercased()
    }
    
    //    func getRangeForCurrentYear() -> String {
    //        let formatter = DateFormatter()
    //        formatter.dateFormat = "yyyy"
    //        let range = ("JAN \(formatter.string(from: CountViewModel.countDate)) ⏤ DEC \(formatter.string(from: CountViewModel.countDate))")
    //        return range
    //    }
    
    func getMonthNumber() -> String {
        let date =  Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
        let formatter = DateFormatter()
        formatter.dateFormat = "M"
        return formatter.string(from: date)
    }
    
    func counterDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.string(from: self)
    }
    
    func dateComponents(from date: Date) -> DateComponents {
        return Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: self, to: date)
    }
    
    func getComponents(from date: Date) -> DateComponents {
        return Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute, .second], from: self, to: date)
    }
    
    func notificationTime(from date: Date) -> String {
        var time = ""
        var str = ""
        let interval = Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: date, to: self)
        if let year = interval.year, year > 0 {
            str = (year > 1 ) ? "years" : "year"
            time = "\(year)" + " " + "\(str)" + " "
        } else if let month = interval.month, month > 0 {
            str = (month > 1 ) ? "months" : "month"
            time =  "\(month)" + " " + "\(str)" + " "
        } else if let week = interval.weekOfMonth, week > 0 {
            str = (week > 1 ) ? "weeks" : "week"
            time =  "\(week)" + " " + "\(str)" + " "
        } else if let day = interval.day, day > 0 {
            str = (day > 1 ) ? "days" : "day"
            time =  "\(day)" + " " + "\(str)" + " "
        } else if let hour = interval.hour, hour > 0 {
            str = (hour > 1 ) ? "hours" : "hour"
            time =  "\(hour)" + " " + "\(str)" + " "
        } else if let minute = interval.minute, minute > 0 {
            str = (minute > 1 ) ? "minutes" : "minute"
            time =  "\(minute)" + " " + "\(str)" + " "
        } else {
            time = "now"
        }
        return time
    }
    
    func taskRemainingTime(from date: Date) -> String {
        var time = ""
        var str = ""
        let interval = Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: self, to: date)
        if let year = interval.year, year > 0 {
            str = (year > 1 ) ? "Years" : "Year"
            time = "\(year)" + " " + "\(str)" + " Left"
        } else if let month = interval.month, month > 0 {
            str = (month > 1 ) ? "Months" : "Month"
            time =  "\(month)" + " " + "\(str)" + " Left"
        } else if let week = interval.weekOfMonth, week > 0 {
            str = (week > 1 ) ? "Weeks" : "Week"
            time =  "\(week)" + " " + "\(str)" + " Left"
        } else if let day = interval.day, day > 0 {
            str = (day > 1 ) ? "Days" : "Day"
            time =  "\(day)" + " " + "\(str)" + " Left"
        } else if let hour = interval.hour, hour > 0 {
            str = (hour > 1 ) ? "Hours" : "Hour"
            time =  "\(hour)" + " " + "\(str)" + " Left"
        } else if let minute = interval.minute, minute > 0 {
            str = (minute > 1 ) ? "Minutes" : "Minute"
            time =  "\(minute)" + " " + "\(str)" + " Left"
        } else {
            time = "Time Over"
        }
        return time
    }
    
    
    func eventRemainingTime(from date: Date) -> String {
        var time = ""
        var str = ""
        let interval = Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: self, to: date)
        if let year = interval.year, year > 0 {
            str = (year > 1 ) ? "Years" : "Year"
            str = "\(year)" + " " + "\(str)" + " "
            time.append(contentsOf: str)
            return time
        }
        if let month = interval.month, month > 0 {
            str = (month > 1 ) ? "Months" : "Month"
            str = "\(month)" + " " + "\(str)" + " "
            time.append(contentsOf: str)
            return time
        }
        if let week = interval.weekOfMonth, week > 0 {
            str = (week > 1 ) ? "Weeks" : "Week"
            str = "\(week)" + " " + "\(str)" + " "
            time.append(contentsOf: str)
            return time
        }
        if let day = interval.day, day > 0 {
            str = (day > 1 ) ? "Days" : "Day"
            str = "\(day)" + " " + "\(str)" + " "
            time.append(contentsOf: str)
            return time
        }
        if let hour = interval.hour, hour > 0 {
            str = (hour > 1 ) ? "Hours" : "Hour"
            str = "\(hour)" + " " + "\(str)" + " "
            time.append(contentsOf: str)
            return time
        }
        if let minute = interval.minute, minute > 0 {
            str = (minute > 1 ) ? "Minutes" : "Minute"
            str = "\(minute)" + " " + "\(str)" + " "
            time.append(contentsOf: str)
            return time
        } else {
            return "Time is passed"
        }
    }
    
    func challengeStatus(from date: Date) -> String {
        var interval = DateComponents()
        var status = ""
        var time = ""
        
        switch self.compare(Date()) {
        case .orderedAscending:
            status = "Ends In "
            interval = Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: self, to: date)
        case .orderedDescending, .orderedSame:
            status = "Start In "
            interval = Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: Date(), to: self)
        }
        
        
        if let year = interval.year, year > 0 {
            time =  status + "\(year)" + " " + "year"
        } else if let month = interval.month, month > 0 {
            time =  status + "\(month)" + " " + "months"
        } else if let week = interval.weekOfMonth, week > 0 {
            time =  status + "\(week)" + " " + "weeks"
        } else if let day = interval.day, day > 0 {
            time =  status + "\(day)" + " " + "days"
        } else if let hour = interval.hour, hour > 0 {
            time =  status + "\(hour)" + " " + "hours"
        } else if let minute = interval.minute, minute > 0 {
            time =  status + "\(minute)" + " " + "minutes"
        } else {
            time = status + "a moment ago"
        }
        return time
    }
}

extension Double {
    func unixToUTC() -> Date {
        let unixTimestamp = self
        return Date(timeIntervalSince1970: unixTimestamp)
    }
    
    var toString: String {
        return String(format: "%0.f", self)
    }
    
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func removeZerosFromEnd() -> String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 16 //maximum digits in Double after dot (maximum precision)
        return String(formatter.string(from: number) ?? "")
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
        //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    var compressImage: Data? {
        return self.jpegData(compressionQuality: 0.5) ?? nil
    }
}

enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    func base64(format: ImageFormat) -> String? {
        var imageData: Data?
        
        switch format {
        case .png: imageData = self.pngData()
        //case .jpeg(let compression): imageData = UIImageJPEGRepresentation(self, compression)
        case .jpeg(let compression): imageData = self.jpegData(compressionQuality: compression)
        }
        
        return imageData?.base64EncodedString()
    }
}


extension FloatingPoint {
    var isInt: Bool {
        return floor(self) == self
    }
}

extension Float {
    func roundTo(places:Int) -> Float {
        let str = String(format: "%.\(places)f", self)
        return Float((str as NSString).floatValue)
    }
}

extension UIColor {
    
    convenience init(hex: String, alpha:CGFloat = 1.0) {
        var hexInt: UInt32 = 0
        let scanner = Scanner(string: hex)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        
        let red = CGFloat((hexInt & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexInt & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexInt & 0xff) >> 0) / 255.0
        let alpha = alpha
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    func isEqualTo(_ color: UIColor) -> Bool {
        var red1: CGFloat = 0, green1: CGFloat = 0, blue1: CGFloat = 0, alpha1: CGFloat = 0
        getRed(&red1, green:&green1, blue:&blue1, alpha:&alpha1)
        
        var red2: CGFloat = 0, green2: CGFloat = 0, blue2: CGFloat = 0, alpha2: CGFloat = 0
        color.getRed(&red2, green:&green2, blue:&blue2, alpha:&alpha2)
        
        return red1 == red2 && green1 == green2 && blue1 == blue2 && alpha1 == alpha2
    }
    func colorFromCode(_ code: Int) -> UIColor {
        
        let red = CGFloat(((code & 0xFF0000) >> 16)) / 255
        let green = CGFloat(((code & 0xFF00) >> 8)) / 255
        let blue = CGFloat((code & 0xFF)) / 255
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}

extension DateFormatter {
    static var formatter = DateFormatter()
}

extension UILabel {
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            
            attributedString.addAttribute(.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            
            attributedText = attributedString
        }
        
        get {
            if let currentLetterSpace = attributedText?.attribute(.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
        
        
    }
    
    
    
    func addInterlineSpacing(spacingValue: CGFloat = 2) {
        self.numberOfLines = 0
        // MARK: - Check if there's any text
        guard let textString = text else { return }
        
        // MARK: - Create "NSMutableAttributedString" with your text
        let attributedString = NSMutableAttributedString(string: textString)
        
        // MARK: - Create instance of "NSMutableParagraphStyle"
        let paragraphStyle = NSMutableParagraphStyle()
        
        // MARK: - Actually adding spacing we need to ParagraphStyle
        paragraphStyle.lineSpacing = spacingValue
        
        paragraphStyle.alignment = .center
        // MARK: - Adding ParagraphStyle to your attributed String
        attributedString.addAttribute(
            .paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length
        ))
        
        // MARK: - Assign string that you've modified to current attributed Text
        attributedText = attributedString
    }
    
    func setLineHeight(lineHeight: CGFloat, withAlignment : NSTextAlignment) {
        let text = self.text
        if let text = text {
            let attributeString = NSMutableAttributedString(string: text)
            let style = NSMutableParagraphStyle()
            style.lineHeightMultiple = lineHeight
            style.alignment = withAlignment
            attributeString.addAttribute(.paragraphStyle, value: style, range: NSMakeRange(0, attributeString.length))
            self.attributedText = attributeString
        }
    }    
}


let imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    
    func makeCircular() {
        self.layer.cornerRadius = self.frame.size.height / 2
        self.layer.masksToBounds = true
    }
    
    func addBorder(_ color:UIColor, _ cornerRadius: CGFloat = 0.0) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
    
    func downloadImage(from url:String, with defaultImage:UIImage?) {
        guard let _ = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            if defaultImage != nil {
                self.image = defaultImage
                self.contentMode = .scaleAspectFit
            } else {
                self.image = nil
            }
            return
        }
        
        let imageURL = URL.init(string: url)
        self.sd_setImage(with: imageURL) { (image, error, _, _) in
            if error != nil {
                self.image = defaultImage
                self.contentMode = .scaleAspectFit
            } else {
                self.image = image
                self.contentMode = .scaleAspectFill
            }
        }
    }
    
    func setImageFromURL(_ url:String, with defaultImage:UIImage?, mode: UIView.ContentMode = .scaleAspectFill) {
        if url.contains("no_image") {
            self.image = defaultImage
            self.contentMode = .scaleAspectFit
            return
        }
        
        let imageLink = "\(APIEnvironment.host)/\(url)"
        guard let urlImage = imageLink.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            if defaultImage != nil {
                self.image = defaultImage
                self.contentMode = .scaleAspectFit
            } else {
                self.image = nil
            }
            return
        }
        
        let imageURL = URL.init(string: urlImage)
        self.sd_setImage(with: imageURL) { (image, error, _, _) in
            if error != nil {
                self.image = defaultImage
                self.contentMode = .scaleAspectFit
            } else {
                self.image = image
                self.contentMode = mode
            }
        }
        
    }
    
    func downloadImageFrom(urlString: String, with placeHolder:UIImage) {
        
        let url = URL(string: urlString)
        self.image = nil
        // check for the cached image first
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        self.image = placeHolder
        // otherwise cache the images to decrease the use of internet, new download of the image will be done
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print(error as Any)
                DispatchQueue.main.async {
                    self.image = UIImage(named: "defaultUser-Male")
                }
                return
            }
            // downloaded the images succesfully
            DispatchQueue.main.async {
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    self.image = downloadedImage
                }
            }
        }.resume()
    }
}

extension Notification.Name {
    static let challengeCreate = Notification.Name("challengeCreate")
    static let challengeUpdate = Notification.Name("challengeUpdate")
    static let challengeAccepted = Notification.Name("challengeAccepted")
    static let updateProfile = Notification.Name("updateProfile")
}

extension UITextField {
    func addDoneButtonOnKeyboard() {
        let toolBar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        toolBar.barStyle = .default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        let items = [flexSpace, doneButton]
        toolBar.items = items
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }
            
            attributedString.addAttribute(.kern,
                                          value: newValue,
                                          range: NSRange(location: 0, length: attributedString.length))
            
            attributedText = attributedString
        }
        
        get {
            if let currentLetterSpace = attributedText?.attribute(.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
    }
}

extension UITextView {
    func addDoneButtonOnKeyboard() {
        let toolBar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        toolBar.barStyle = .default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        let items = [flexSpace, doneButton]
        toolBar.items = items
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
}

extension UIPickerView {
    var toolBar:UIToolbar { return UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))  }
    func addDoneButtonOnKeyboard() {
        let toolBar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        toolBar.barStyle = .default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        let items = [flexSpace, doneButton]
        toolBar.items = items
        toolBar.sizeToFit()
    }
    open override var inputView: UIView? {
        addDoneButtonOnKeyboard()
        return toolBar
    }
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
}

extension UIView {
    
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func addShadow() {
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.5)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 10
    }
    
    func showAnimated() {
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    
    func hideAnimated() {
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
    
    func updateViewAnimated( _ cns:NSLayoutConstraint, _ show:Bool) {
        if show {
            UIView.animate(withDuration: 2.0, delay: 0, options: [.curveEaseIn],
                           animations: {
                            cns.constant = 50
                            self.layoutIfNeeded()
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.0, delay: 0, options: [.curveEaseIn],
                           animations: {
                            cns.constant = -1000
                            self.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func lock() {
        if let _ = viewWithTag(10) {
            //View is already locked
        }
        else {
            let lockView = UIView(frame: bounds)
            lockView.backgroundColor = UIColor(white: 0.0, alpha: 0.75)
            lockView.tag = 10
            lockView.alpha = 0.0
            let activity = UIActivityIndicatorView(style: .white)
            activity.hidesWhenStopped = true
            activity.center = lockView.center
            lockView.addSubview(activity)
            activity.startAnimating()
            addSubview(lockView)
            
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 1.0
            })
        }
    }
    
    func unlock() {
        if let lockView = viewWithTag(10) {
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 0.0
            }, completion: { finished in
                lockView.removeFromSuperview()
            })
        }
    }
    
    func fadeOut(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
    func fadeIn(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as? UIView
    }
    
    func animateLineView(_ btn: UIButton, _ cns:NSLayoutConstraint, _ viewLine:UIView) {
        UIView.animate(withDuration: 0.15) {
            var frame = viewLine.frame
            frame.origin.x = btn.frame.origin.x
            cns.constant = btn.frame.origin.x
            viewLine.frame = frame
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var leftBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: newValue, height: bounds.height))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = UIColor(cgColor: layer.borderColor!)
            line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line(==lineWidth)]", options: [], metrics: metrics, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line]|", options: [], metrics: nil, views: views))
        }
    }
    
    @IBInspectable var topBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: bounds.width, height: newValue))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line]|", options: [], metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line(==lineWidth)]", options: [], metrics: metrics, views: views))
        }
    }
    
    @IBInspectable var rightBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: bounds.width, y: 0.0, width: newValue, height: bounds.height))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "[line(==lineWidth)]|", options: [], metrics: metrics, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line]|", options: [], metrics: nil, views: views))
        }
    }
    @IBInspectable var bottomBorderWidth: CGFloat {
        get {
            return 0.0   // Just to satisfy property
        }
        set {
            let line = UIView(frame: CGRect(x: 0.0, y: bounds.height, width: bounds.width, height: newValue))
            line.translatesAutoresizingMaskIntoConstraints = false
            line.backgroundColor = borderColor
            line.tag = 110
            self.addSubview(line)
            
            let views = ["line": line]
            let metrics = ["lineWidth": newValue]
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[line]|", options: [], metrics: nil, views: views))
            addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[line(==lineWidth)]|", options: [], metrics: metrics, views: views))
        }
    }
    func removeborder() {
        for view in self.subviews {
            if view.tag == 110  {
                view.removeFromSuperview()
            }
        }
    }
    
    @IBInspectable var isShadow: Bool {
        get {
            return false
        }
        set {
            self.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(0.0))
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowRadius = 3
            self.layer.shadowOpacity = 0.35
            self.layer.masksToBounds = false
            self.clipsToBounds = true
        }
    }
    
    func setShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 3
        layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        layer.shouldRasterize = true
    }
    
    func addGradientLayer(gradientLayer:CAGradientLayer, colors:[CGColor], location:[NSNumber]) {
        gradientLayer.frame = self.bounds
        gradientLayer.colors = colors
        gradientLayer.locations = location
        self.layer.addSublayer(gradientLayer)
    }
    
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

extension UIButton {
    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        let writingDirection = UIApplication.shared.userInterfaceLayoutDirection
        let factor: CGFloat = writingDirection == .leftToRight ? 1 : -1
        
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount*factor, bottom: 0, right: insetAmount*factor)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount*factor, bottom: 0, right: -insetAmount*factor)
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
    }
}

extension Notification.Name {
    static let reloadClusterList = Notification.Name("reloadClusterList")
    static let reloadActivityList = Notification.Name("reloadActivityList")
    static let reloadActivityDetail = Notification.Name("reloadActivityDetail")
    static let reloadTaskList = Notification.Name("reloadTaskList")
    static let reloadTaskDetail = Notification.Name("reloadTaskDetail")
    static let reloadEventsList = Notification.Name("reloadEventsList")
    static let reloadEventsDetail = Notification.Name("reloadEventsDetail")
    static let reloadShoppingList = Notification.Name("reloadShoppingList")
    static let reloadShoppingDetail = Notification.Name("reloadShoppingDetail")
    static let selectedShoppingList = Notification.Name("selectedShoppingList")
    static let selectedGroupMember = Notification.Name("selectedGroupMember")
    static let selectedGroupMemberForTask = Notification.Name("selectedGroupMemberForTask")
    static let reloadContactsList = Notification.Name("reloadContactsList")
    static let selectedMyTask = Notification.Name("selectedMyTask")
    static let selectedMyList = Notification.Name("selectedMyList")
    static let userDataUpdate = Notification.Name("userDataUpdate")
    static let userProfileImageUpdate = Notification.Name("userProfileImageUpdate")
    static let reloadClusterDetail = Notification.Name("reloadClusterDetail")
    static let reloadClusterDetailOnTimeline = Notification.Name("reloadClusterDetailOnTimeline")
    static let reloadCalendarEventsList = Notification.Name("reloadCalendarEventsList")
    static let reloadCategoryOnCreation = Notification.Name("reloadCategoryOnCreation")
    static let reloadTaskUpdateList = Notification.Name("reloadTaskUpdateList")
    static let reloadShoppingListItems = Notification.Name("reloadShoppingListItems")
    static let reloadTimelineOnCreatePost = Notification.Name("reloadTimelineOnCreatePost")
    static let reloadTimelineOnComment = Notification.Name("reloadTimelineOnComment")
    static let reloadCountdownList = Notification.Name("reloadCountdownList")
    static let reloadNotificationBadgeCount = Notification.Name("reloadNotificationBadgeCount")
}


class ColumnFlowLayout: UICollectionViewFlowLayout {
    
    let cellsPerRow: Int
    
    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()
        
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else { return }
        let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        itemSize = CGSize(width: itemWidth, height: itemWidth)
    }
    
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
    
}

extension Dictionary where Key == String, Value: Equatable {
    func key(for value: Value) -> Key? {
        return compactMap { value == $1 ? $0 : nil }.first
    }
}

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
}

extension UIFont {
    
    public enum RobotoType: String {
        case semiboldItalic = "-SemiboldItalic"
        case semibold = "-Semibold"
        case regular = ""
        case lightItalic = "-LightItalic"
        case light = "-Light"
        case italic = "-Italic"
        case extraBold = "-Extrabold"
        case boldItalic = "-BoldItalic"
        case bold = "-Bold"
    }
    
    static func Roboto(_ type: RobotoType = .regular, size: CGFloat = UIFont.systemFontSize) -> UIFont {
        return UIFont(name: "Roboto\(type.rawValue)", size: size)!
    }
    
    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }
    
    var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
    
}

extension Data {
    private static let mimeTypeSignatures: [UInt8 : String] = [
        0xFF : "image/jpeg",
        0x89 : "image/png",
        0x47 : "image/gif",
        0x49 : "image/tiff",
        0x4D : "image/tiff",
        0x25 : "application/pdf",
        0xD0 : "application/vnd",
        0x46 : "text/plain",
    ]
    
    var mimeType: String {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return Data.mimeTypeSignatures[c] ?? "application/octet-stream"
    }
    
    init<T>(fromArray values: [T]) {
        var values = values
        self.init(buffer: UnsafeBufferPointer(start: &values, count: values.count))
    }
    
    func toArray<T>(type: T.Type) -> [T] {
        return self.withUnsafeBytes {
            [T](UnsafeBufferPointer(start: $0, count: self.count/MemoryLayout<T>.stride))
        }
    }
}

extension URL {    
    func openBrowser() {
        if UIApplication.shared.canOpenURL(self) {
            UIApplication.shared.open(self, options: [:], completionHandler: nil)
        }
    }
}


extension Decimal {
    func rounded(_ roundingMode: NSDecimalNumber.RoundingMode = .bankers) -> Decimal {
        var result = Decimal()
        var number = self
        NSDecimalRound(&result, &number, 0, roundingMode)
        return result
    }
    var whole: Decimal { self < 0 ? rounded(.up) : rounded(.down) }
    var fraction: Decimal { self - whole }
}

extension Character {
    /// A simple emoji is one scalar and presented to the user as an Emoji
    var isSimpleEmoji: Bool {
        guard let firstScalar = unicodeScalars.first else { return false }
        return firstScalar.properties.isEmoji && firstScalar.value > 0x238C
    }

    /// Checks if the scalars will be merged into an emoji
    var isCombinedIntoEmoji: Bool { unicodeScalars.count > 1 && unicodeScalars.first?.properties.isEmoji ?? false }

    var isEmoji: Bool { isSimpleEmoji || isCombinedIntoEmoji }
}

extension Int {
    var digits: Int {
        return (String(describing: self).compactMap { Int(String($0)) }).count
    }
}
