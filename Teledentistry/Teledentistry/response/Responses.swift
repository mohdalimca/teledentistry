//
//  Responses.swift
//  Clustry
//
//  Created by cis on 14/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

struct SuccessResponseModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: DataClass?
}

struct DataClass: Codable {
    var login: Login?
    var socialLogin:SocialLogin?
    let register: Register?
    let forgot: Forgot?
    let profile: Profile?
//    let searchUsers: SearchUsers?
    
    enum CodingKeys: String, CodingKey {
        case login
        case socialLogin = "data"
        case register
        case forgot
        case profile
//        case searchUsers = "data"
    }
}


// MARK: - Version
struct Version: Codable {
    let status, versioncode: Int?
    let versionmessage, currentVersion: String?

    enum CodingKeys: String, CodingKey {
        case status, versioncode, versionmessage
        case currentVersion = "current_version"
    }
}

// MARK: - Login
struct Login: Codable {
    let authorization, userID, userEmail, fullName: String?
    let userRole: String?
    let userPic: String?

    enum CodingKeys: String, CodingKey {
        case authorization = "Authorization"
        case userID = "user_id"
        case userEmail = "user_email"
        case fullName = "full_name"
        case userRole = "user_role"
        case userPic = "user_pic"
    }
}

// MARK: - Social Login
struct SocialLogin: Codable {
    let authorization, fullName, userEmail, userID: String?
    let userRole: String?
    let userPic: String?

    enum CodingKeys: String, CodingKey {
        case authorization = "Authorization"
        case fullName = "full_name"
        case userEmail = "user_email"
        case userID = "user_id"
        case userRole = "user_role"
        case userPic = "user_pic"
    }
}

// MARK: - Register
struct Register: Codable {
    let userID: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
    }
}

// MARK: - Forgot
struct Forgot: Codable {
    let userID, password: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case password = "Password"
    }
}

// MARK: - Profile
struct Profile: Codable {
    let id, userEmail, fullName, countryCode: String?
    let userPhone, dob, gender, userRole: String?
    let height, weight, insurance, policy: String?
    let userPic: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userEmail = "user_email"
        case fullName = "full_name"
        case countryCode = "country_code"
        case userPhone = "user_phone"
        case dob, gender
        case userRole = "user_role"
        case height, weight, insurance, policy
        case userPic = "user_pic"
    }
}

struct SearchUsers: Codable {
    let totalcount: String?
    let record: [Record]?
}

// MARK: - Record
struct Record: Codable {
    let id, fullName: String?
    let userPic: String?
    let userPhone: String?

    enum CodingKeys: String, CodingKey {
        case id
        case fullName = "full_name"
        case userPic = "user_pic"
        case userPhone = "user_phone"
    }
}
