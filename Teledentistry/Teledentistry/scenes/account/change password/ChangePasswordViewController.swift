//
//  ChangePasswordViewController.swift
//  Clustry
//
//  Created by cis on 15/11/19.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController { }
    
//    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var lblOld: UILabel!
//    @IBOutlet weak var lblNew: UILabel!
//    @IBOutlet weak var lblConfirm: UILabel!
//    
//    @IBOutlet weak var txtOld: UITextField!
//    @IBOutlet weak var txtNew: UITextField!
//    @IBOutlet weak var txtConfirm: UITextField!
//    
//    @IBOutlet weak var viewOld: UIView!
//    @IBOutlet weak var viewNew: UIView!
//    @IBOutlet weak var viewConfirm: UIView!
//    
//    @IBOutlet weak var btnBack: UIButton!
//    @IBOutlet weak var btnSave: UIButton!
//    @IBOutlet weak var btnOldEye: UIButton!
//    @IBOutlet weak var btnNewEye: UIButton!
//    @IBOutlet weak var btnConfirmEye: UIButton!
//    
//    let viewModel = ChangePasswordViewModel(provider: OnboardingServiceProvider())
//    weak var router: NextSceneDismisser?
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setup()
//    }
//    
//    private func setup() {
//        [btnBack, btnSave, btnOldEye, btnNewEye, btnConfirmEye].forEach {
//            $0?.layer.cornerRadius = ($0?.frame.size.height)! / 2
//            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
//        }
//        [txtOld, txtNew, txtConfirm].forEach {
//            $0?.delegate = self
//            $0?.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
//            $0?.letterSpace = 5.0
//        }
//        viewModel.view = self
//        [viewOld, viewNew, viewConfirm].forEach {
//            $0?.borderColor = .lightGray
//        }
//    }
//    
//    @objc func textFieldDidChange(_ textField: UITextField) {
//        
//        switch textField {
//        case txtOld:
//            viewModel.old = textField.text!
//        case txtNew:
//            viewModel.new = textField.text!
//        case txtConfirm:
//            viewModel.confirm = textField.text!
//        default:break
//        }
//        textField.letterSpace = 5.0
//    }
//    
//    @objc func buttonPressed(_ sender: UIButton) {
//        switch sender {
//        case btnBack:
//            router?.dismiss(controller: .changePassword)
//        case btnSave:
//            startAnimation()
//            viewModel.onAction(action: .inputComplete(.forgot), for: .forgot)
//        case btnOldEye:
//            txtOld.isSecureTextEntry = btnOldEye.isSelected
//            btnOldEye.isSelected = !btnOldEye.isSelected
//        case btnNewEye:
//            txtNew.isSecureTextEntry = btnNewEye.isSelected
//            btnNewEye.isSelected = !btnNewEye.isSelected
//        case btnConfirmEye:
//            txtConfirm.isSecureTextEntry = btnConfirmEye.isSelected
//            btnConfirmEye.isSelected = !btnConfirmEye.isSelected
//        default:
//            break
//        }
//    }
//}
//
//extension ChangePasswordViewController: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        switch textField {
//        case txtOld:
//            viewOld.borderColor = .orange
//            viewModel.old = textField.text!
//        case txtNew:
//            viewNew.borderColor = .orange
//            viewModel.new = textField.text!
//        case txtConfirm:
//            viewConfirm.borderColor = .orange
//            viewModel.confirm = textField.text!
//        default:break
//        }
//    }
//    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        switch textField {
//        case txtOld:
//            viewOld.borderColor = .lightGray
//            viewModel.old = textField.text!
//        case txtNew:
//            viewNew.borderColor = .lightGray
//            viewModel.new = textField.text!
//        case txtConfirm:
//            viewConfirm.borderColor = .lightGray
//            viewModel.confirm = textField.text!
//        default:break
//        }
//    }
//    
//}
//
//extension ChangePasswordViewController: OnboardingViewRepresentable {
//    func onAction(_ action: OnboardingAction) {
//        switch action {
//        case let .errorMessage(text), let .requireFields(text: text):
//            stopAnimating()
//            showSnackBar(text)
//        case .forgot:
//            stopAnimating()
//            showSnackBar((viewModel.response?.message)!)
//            router?.dismiss(controller: .changePassword)
//        default:
//            break
//        }
//    }
//}
