//
//  ChangePasswordViewModel.swift
//  Clustry
//
//  Created by cis on 15/11/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

final class ChangePasswordViewModel {
    var old = ""
    var new = ""
    var confirm = ""
    var response: SuccessResponseModel?
    var provider: OnboardingServiceProvidable
    var onboarding = OnboardingViewModel()
    weak var view: OnboardingViewRepresentable?
    
    init(provider: OnboardingServiceProvidable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    func onAction(action: OnboardingAction, for screen: OnboardingScreenType) {
        switch action {
        case .inputComplete(.forgot): validate()
        default: break
        }
    }
    
    func validate() {
        if old == "" {
            view?.onAction(.requireFields(text: "Old password is required"))
            return
        }
        
        if new == "" {
            view?.onAction(.requireFields(text: "New password is required"))
            return
        }
        
        if confirm == "" {
            view?.onAction(.requireFields(text: "Confirm password is required"))
            return
        }
        
        if new != confirm {
            view?.onAction(.requireFields(text: "Password mismatch"))
            return
        }
        provider.changePassword(oldPassword: old, newPassword: confirm)
    }
}

extension ChangePasswordViewModel: OnboardingServiceProvierDelegate, InputViewDelegate {
    func completed<T>(for action: OnboardingAction, with response: T?, with error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.message else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                if let resp = response as? SuccessResponseModel, resp.status != 0 {
                    self.view?.onAction(.errorMessage(resp.message ?? errorMessage))
                } else {
                    self.response = response as? SuccessResponseModel
                    self.view?.onAction(.forgot)
                }
            }
        }
    }
}
