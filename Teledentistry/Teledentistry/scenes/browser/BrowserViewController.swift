//
//  BrowserViewController.swift
//  Clustry
//
//  Created by cis on 17/08/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit
import WebKit

class BrowserViewController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var browser: WKWebView!
    
    weak var router: NextSceneDismisser?
    var browserURL:URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
    }

    private func setup() {
        startAnimation()
        Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(removeAnimation), userInfo: nil, repeats: false)
        browser.navigationDelegate = self
        [btnBack].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        let request = URLRequest(url: browserURL)
        browser.load(request)
    }
    
    @objc func removeAnimation() {
        stopAnimating()
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        router?.dismiss(controller: .browser)
    }
}

extension BrowserViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        stopAnimating()
    }
}
