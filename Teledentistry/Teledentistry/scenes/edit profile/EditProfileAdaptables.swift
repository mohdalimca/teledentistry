//
//  EditProfileAdaptables.swift
//  Clustry
//
//  Created by cis on 13/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

protocol EditProfileTableViewDelegate: class {
}

protocol EditProfileInputDelegate:class {
    func onAction(action:ProfileAction, and scene:Scenes, for screen:ProfileScreen)
}

protocol EditProfileViewRepresentable:class {
    func sendAction(_ action: ProfileButtonAction)
}
