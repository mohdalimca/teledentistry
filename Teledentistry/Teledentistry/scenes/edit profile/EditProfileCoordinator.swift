//
//  EditProfileCoordinator.swift
//  Clustry
//
//  Created by cis on 13/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation


final class EditProfileCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: EditProfileViewController = EditProfileViewController.from(from: .main, with: .editProfile)

    
    override func start() {
        super.start()
        router.setRootModule(controller, hideBar: true)
    }
}

extension EditProfileCoordinator: NextSceneDismisser {
    
    func push(scene: Scenes) {
//        switch scene {
//        case .imageDetail: startImageDetail()
//        case .landing: break
//        default: break
//        }
    }
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension EditProfileCoordinator: CoordinatorDimisser {
    
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}


