//
//  EditProfileViewController.swift
//  Clustry
//
//  Created by cis on 13/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    
//    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var btnBack: UIButton!
//    @IBOutlet weak var btnSave: UIButton!
//    
//    weak var router: NextSceneDismisser?
//    let viewModel = EditProfileViewModel(provider: ProfileServiceProvider())
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setup()
//    }
//    
//    private func setup() {
//        updateCountryCode()
//        viewModel.view = self
//        [btnBack, btnSave].forEach {
//            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
//        }
//        populate()
//    }
//    
//    
//    
//    private func navigateToImageDetail( _ index:Int) {
//        viewModel.selectedIndex = index
////        router?.push(scene: .imageDetail)
//    }
//    
//    private func didTapCountry() {
//        present(countryList, animated: true, completion: nil)
//    }
//    
//    
//    private func showWarningAlert() {
//        let alert = UIAlertController(title: "Warning", message: "Do you want to proceed, all data will be lost?", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
//            self.view.endEditing(true)
//            self.router?.dismiss(controller: .editProfile)
//        }))
//        present(alert, animated: true, completion: nil)
//    }
//    
//    @objc func buttonPressed(_ sender:UIButton) {
//        if sender == btnBack {
//            if viewModel.showWarning == true {
//                showWarningAlert()
//            } else {
//                router?.dismiss(controller: .editProfile)
//            }
//        } else {
//            startAnimation()
//            viewModel.saveProfile()
//        }
//    }
//    
//    private func populate() {
//        var strName = "-"
//        if let _ = viewModel.user.firstName {
//            strName = ""
//            strName.append(viewModel.user.firstName! + " ")
//        }
//        
//        if let _ = viewModel.user.lastName {
//            strName.append(viewModel.user.lastName!)
//        }
//        viewModel.fullName = strName
//        viewModel.email = viewModel.user.email
//        viewModel.phone = viewModel.user.phone ?? "-"
//        viewModel.address = viewModel.user.address ?? "-"
//        viewModel.dobTimeStamp = viewModel.user.dob?.toDouble ?? 0
//        self.getAttachmentsURL()
////        if viewModel.user.attachment != nil {
////            viewModel.allAttachments.removeAll()
////            var i = 0
////            for attachment in viewModel.user.attachment! {
////                let imageDict: [String:Any] = [
////                    multiPartImageKey : attachment.attachment ?? "",
////                    multiPartImageNameKey : "attachment\(i + 1)",
////                    multiPartFileName: attachment.docName ?? "image.jpg",
////                    "id": attachment.id ?? 0
////                ]
////
////                viewModel.allAttachments.append(imageDict)
////                i = i + 1
////            }
////            DispatchQueue.main.async {
////                self.tableView.reloadSections(IndexSet(integer: 2), with: .fade)
////            }
////        }
//    }
//    
//    private func getAttachmentsURL() {
//        if viewModel.user.attachment != nil {
//            viewModel.allAttachments.removeAll()
//            for temp in viewModel.user.attachment! {
//                viewModel.allAttachments.append(temp.attachment ?? "")
//            }
//        }
//    }
//
//    private func showDatePicker(_ scene:Scenes, isReminderDate: Bool?) {
//        let picker: DatePicker = DatePicker.from(from: .main, with: .datePicker)
//        picker.delegate = self
//        picker.selectedValue = ""
//        picker.mode = .date
//        picker.scene = scene
//        picker.modalTransitionStyle = .crossDissolve
//        picker.modalPresentationStyle = .overFullScreen
//        self.present(picker, animated: true, completion: nil)
//    }
//    
//    private func onTapAction(_ field: String) {
//        if field == "DOB" {
//            self.view.endEditing(true)
//            
//        }
//    }
//    
//    private func onTapAction(_ scene: Scenes) {
////        switch scene {
////        case .editDOB :
////            self.showDatePickerOn(controller: self, scene: scene, pickerMode: .date, setMinDate: nil, setMaxDate: true)
//////            showDatePicker(scene, isReminderDate: nil)
////        default:
////            break
////        }
//    }
//    
//    private func dismiss(_ text:String) {
//        NotificationCenter.default.post(name: .userDataUpdate, object: self)
//        stopAnimating()
//        showSnackBar(text)
//        router?.dismiss(controller: .editProfile)
//    }
//    
//    private func dismiss() {
//        NotificationCenter.default.post(name: .userDataUpdate, object: self)
//        stopAnimating()
//        router?.dismiss(controller: .editProfile)
//    }
//    
//    private func userProfileImageUpdate() {
//        NotificationCenter.default.post(name: .userProfileImageUpdate, object: nil, userInfo: ["profile_image" : viewModel.profileImageURL!])
//        stopAnimating()
//    }
//    
//    private func imageDidPicked(_ image:UIImage) {
//        if viewModel.isPorfilePictureSelected {
//            viewModel.updateProfileImage()
//            tableView.reloadSections(IndexSet(integer: 0), with: .none)
//        } else {
//            DispatchQueue.main.async {
//                self.showInputAlert(controller: self, type: .title)
//            }
//        }
//    }
//    
//    private func updateCountryCode() {
//        var strCountry = ""
//        strCountry.append(viewModel.selectedCountry.flag! + " ")
//        strCountry.append(viewModel.selectedCountry.countryCode + " +")
//        strCountry.append(viewModel.selectedCountry.phoneExtension)
//        viewModel.countryCode = strCountry
//        tableView.reloadSections(IndexSet(integer: 1), with: .none)
//    }
//    
//    private func reloadOnAttachment(_ image:[UIImage]?, _ url:URL?, _ name:String?) {
//        viewModel.showWarning = true
//        viewModel.allAttachments.append(contentsOf: image!)
//        tableView.reloadSections(IndexSet(integer: 2), with: .automatic)
//    }
//}
//
//extension EditProfileViewController: GetDateDelagate {
//    func getDate(_ date: String, index: Int, scene: Scenes, timestamp: TimeInterval) {
//        viewModel.dateOfBirth = date
//        viewModel.dobTimeStamp = timestamp
//        tableView.reloadSections(IndexSet(integer: 1), with: .none)
//    }
//    
//    func getValue(_ value: String, index: Int, scene: Scenes?) {
//        
//    }
//}
//
//extension EditProfileViewController: EditProfileViewRepresentable {
//    func sendAction(_ action: ProfileButtonAction) {
//        switch action {
//        case .camera:
//            imagePicker.present(from: self.view)
//        case .save:
//            startAnimation()
//            viewModel.saveProfile()
//        default:
//            break
//        }
//    }
//}
//
//extension EditProfileViewController: CountryListDelegate, InputFieldAlertDelegate {
//    func selectedCountry(country: Country) {
//        viewModel.selectedCountry = country
//        updateCountryCode()
//    }
//    
//    
//    func userInput(_ text: String) {
//        var name = "NA"
//        if !text.trimmingCharacters(in: .whitespaces).isEmpty {
//            name = text
//        }
//        viewModel.allAttachments.append([multiPartImageKey : viewModel.selectedImage!, multiPartFileName:name])
//        tableView.reloadSections(IndexSet(integer: 2), with: .automatic)
//    }
//}
//
//extension EditProfileViewController: ProfileViewRepresentable {
//    func onAction(_ action: ProfileAction) {
//        switch action {
//        case let .successMessage(text):
//            dismiss(text)
//        case let .imageChoosen(image):
//            imageDidPicked(image)
//        case let .didTapField(field):
//            onTapAction(field)
//        case let .didTapScene(scene):
//            onTapAction(scene)
//        case let .requireFields(text: text), let .errorMessage(text):
//            showSnackBar(text)
//            stopAnimating()
//        case .updateImage:
//            userProfileImageUpdate()
//        case .update:
//            dismiss()
//        case let .chooseAttachment(image, url, name):
//            reloadOnAttachment(image, url, name)
//        default:
//            stopAnimating()
//        }
//    }
}
