//
//  EditProfileViewModel.swift
//  Clustry
//
//  Created by cis on 13/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit
import Alamofire

final class EditProfileViewModel {
//    private var items: [RowSectionDisplayable] = []
//    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
//    var user: User!
//    let provider: ProfileServiceProvidable
//    var view: ProfileViewRepresentable?
//    
//    var selectedCountry: Country!
//    var fullName: String?
//    var email: String?
//    var countryCode:String = ""
//    var phone: String?
//    var address: String?
//    var dateOfBirth: String?
//    var deleteAttachment:String?
//    var dobTimeStamp: Double = 0
//    var selectedAttachment = [UIImage]()
//    var selectedAttachmentName = [String]()
//    var selectedAttachmentURL = [String]()
//    var selectedImage: UIImage?
//    var isPorfilePictureSelected = true
//    var profileImageURL:String?
//    var deleteAttachmentId = [String]()
//    var imageArray = [[String:Any]]()
//    var fileArray = [[String:Any]]()
//    var imageDict : [String : Any] = [:]
//    var deleteIDs = [Int]()
//    var selectedIndex: Int = 0
//    var attachmentImages = [String]()
//    var allAttachments = [Any]()
//    var showWarning = false
//    
//    init(provider: ProfileServiceProvidable) {
//        items = RowSectionDisplayableFunction.makeRowSectionDisplayable(resource: "EditProfile")
//        self.provider = provider
//        self.provider.delegate = self
//    }
//    
//    var count:Int {
//        return items.count
//    }
//    
//    func items(in section: Int) -> [Row] {
//        return items[section].content
//    }
//    
//    func item(at section: Int) -> RowSectionDisplayable {
//        return items[section]
//    }
//    
//    func item(for indexPath: IndexPath) -> Row {
//        return items(in: indexPath.section)[indexPath.row]
//    }
//    
//    func saveProfile() {
//        validate()
//    }
//    
//    func updatePorfileImage() {
//        provider.updateUserImage(param: UserParams.ProfileImage(images: selectedImage?.compressImage))
//    }
//    
//    private func setField(field:String, input:String, and scene:Scenes) {
//        switch scene {
//        case .editFullName:
//            fullName = input
//            user.username = fullName
//        case .editEmail:
//            email = input
//            user.email = email
//        case .editPhone:
//            phone = input
//            user.phone = phone
//        case .editAddress:
//            address = input
//            user.address = address
//        default:
//            break
//        }
//    }
//    
//    func updateProfileImage() {
//        let param = UserParams.ProfileImage(images:nil)
//        let images = apiAttachments(type: .image, allAttachments: allAttachments, attachmentKey: "image")
//        WebService().requestMultiPart(urlString: APIURL.updateProfileImage, httpMethod: .post, parameters: param, decodingType: SuccessResponseModel.self, imageArray: images, fileArray: []) { (response, error) in
//            if error != nil {
//                self.view?.onAction(.errorMessage(error ?? errorMessage))
//            } else {
//                if let resp = response as? SuccessResponseModel {
//                    if (resp.status)! {
//                        self.profileImageURL = resp.data?.updateProfileURL ?? ""
//                        self.view?.onAction(.updateImage)
//                    } else {
//                        self.view?.onAction(.errorMessage(resp.message ?? errorMessage))
//                    }
//                    
//                } else{
//                    self.view?.onAction(.errorMessage(error ?? errorMessage))
//                }
//            }
//        }
//    }
//    
//    func apiCall() {
////        for i in 0..<allAttachments.count {
////            let temp = allAttachments[i]
////            if let _ = temp[multiPartImageKey] as? UIImage    {
////                imageDict = [multiPartImageKey : temp[multiPartImageKey] as! UIImage,
////                             multiPartImageNameKey : "attachment\(i + 1)",
////                    multiPartFileName: temp[multiPartFileName] as? String ?? "image.jpg"
////                ]
////                imageArray.append(imageDict)
////            } else {
////                //                fileArray.append(temp as! [String:Any])
////            }
////        }
//        
//        let images = apiAttachments(type: .image, allAttachments: allAttachments, attachmentKey: "attachment")
//        let deleteIDs = deleteAttachmentId.joined(separator: ",")
//        let param = ProfileUpdatable(name: fullName, phone: phone, address: address, dob: dobTimeStamp, delete_attachment: deleteIDs, isd_code: selectedCountry.phoneExtension)
//        
//        WebService().requestMultiPart(urlString: APIURL.updateProfile, httpMethod: .post, parameters: param, decodingType: SuccessResponseModel.self, imageArray: images, fileArray: []) { (response, error) in
//            if error != nil {
//                self.view?.onAction(.errorMessage(error ?? errorMessage))
//            } else {
//                if let resp = response as? SuccessResponseModel {
//                    !(resp.status)! ? self.view?.onAction(.errorMessage(resp.message ?? errorMessage)) : self.view?.onAction(.update)
//                } else{
//                    self.view?.onAction(.errorMessage(error ?? errorMessage))
//                }
//            }
//        }
//    }
//    
//    private func validate() {
//        switch "-" {
//        case fullName:
//            view?.onAction(.requireFields(text: "User Name is required"))
//        case email:
//            view?.onAction(.requireFields(text: "Email is required"))
//        case phone:
//            view?.onAction(.requireFields(text: "Phone number is required"))
////        case address:
////            view?.onAction(.requireFields(text: "Address is required"))
////        case dateOfBirth:
////            view?.onAction(.requireFields(text: "Date of birth is required"))
//        default:
//            apiCall()
//        }
//    }
//}
//
//extension EditProfileViewModel: EditProfileInputDelegate {
//    func onAction(action: ProfileAction, and scene:Scenes, for screen: ProfileScreen) {
//        if screen == .edit {
//            switch action {
//            case let .editingDidEnd(field, input):
//                setField(field: field, input: input, and: scene)
//            case let .didTapField(field):
//                didTap(field: field)
//            case let .didTapScene(scene):
//                didTapScene(scene: scene)
//            case .inputComplete(screen):
//                validate()
//            default:
//                break
//            }
//        }
//    }
//    
//    private func didTapScene(scene:Scenes) {
//        self.view?.onAction(.didTapScene(scene))
//    }
//    
//    private func didTap(field:String) {
//        self.view?.onAction(.didTapField(field))
//    }
//}
//
//extension EditProfileViewModel: ImagePickerDelegate {
//    func didSelect(image: UIImage?) {
//        guard let image = image else { return }
//        print("Image === \(image)")
//        selectedImage = image
//        self.view?.onAction(.imageChoosen(image))
//    }
//}
//
//extension EditProfileViewModel: AttachmentPickerDelegate {
//    
//    func didSelectImage(image: [UIImage]?) {
//        guard let img = image else { return }
//        print("Image === \(img.count)")
//        self.view?.onAction(.chooseAttachment(image, nil, nil))
//    }
//    
//    func didSelectVideo(url: URL?) {  }
//    
//    func didSelectDocuments(url: URL?) {  }
//}
//
//extension EditProfileViewModel: ProfileServiceProviderDelegate {
//    func completed<T>(for action: ProfileAction, with response: T?, error: APIError?) {
//        DispatchQueue.main.async {
//            if error != nil {
//                self.view?.onAction(.errorMessage(errorMessage))
//            } else {
//                if let _ =  response as? SuccessResponseModel {
//                    switch action {
//                    case .updateImage:
//                        //                            self.profileImageURL = resp.data
//                        break
//                    default: break
//                    }
//                    self.view?.onAction(action)
//                } else {
//                    self.view?.onAction(.errorMessage(errorMessage))
//                }
//            }
//        }
//    }
}
