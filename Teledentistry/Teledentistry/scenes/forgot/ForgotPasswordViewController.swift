//
//  ForgotPasswordViewController.swift
//  Clustry
//
//  Created by cis on 13/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController { }
    
//    @IBOutlet weak var btnBack: UIButton!
//    @IBOutlet weak var btnReset: UIButton!
//    @IBOutlet weak var txtEmail: UITextField!
//    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var lblEmail: UILabel!
//    @IBOutlet weak var viewEmail: UIView!
//
//    weak var router: NextSceneDismisser?
//    private let viewModel = ForgotPasswordViewModel(provider: OnboardingServiceProvider())
//    var welcome: Bool!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setup()
//    }
//
//    private func setup() {
//        [btnBack, btnReset].forEach {
//            $0?.layer.cornerRadius = ($0?.frame.size.height)! / 2
//            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
//        }
//        viewModel.view = self
//        txtEmail.delegate = self
//        viewEmail.borderColor = .lightGray
//        txtEmail.addTarget(self, action: #selector(textFieldDidChanged(textField:)), for: .editingChanged)
//
//    }
//
//    @objc func buttonPressed(_ sender: UIButton) {
//        if sender == btnBack {
//            router?.dismiss(controller: .forgot)
//        } else {
//            startAnimation()
//            viewModel.onAction(action: .inputComplete(.forgot), for: .forgot)
//        }
//    }
//
//    @objc func textFieldDidChanged(textField: UITextField) {
//        viewModel.email = textField.text!
//    }
//}
//
//extension ForgotPasswordViewController: UITextFieldDelegate {
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        viewEmail.borderColor = .orange
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        viewEmail.borderColor = .lightGray
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if let _ = string.rangeOfCharacter(from: .whitespacesAndNewlines) {
//            return false
//        }
//        return true
//    }
//}
//
//extension ForgotPasswordViewController: OnboardingViewRepresentable {
//    func onAction(_ action: OnboardingAction) {
//        switch action {
//        case let .errorMessage(text), let .requireFields(text: text):
//            stopAnimating()
//            showSnackBar(text)
//        case .forgot:
//            stopAnimating()
//            showSnackBar(viewModel.response.message!)
//            router?.dismiss(controller: .forgot)
//        default:
//            break
//        }
//    }
//}
