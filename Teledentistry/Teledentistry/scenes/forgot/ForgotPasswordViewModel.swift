//
//  ForgotPasswordViewModel.swift
//  Clustry
//
//  Created by cis on 13/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

final class ForgotPasswordViewModel {
    var email = ""
    var response: SuccessResponseModel!
    let provider: OnboardingServiceProvidable
    weak var view: OnboardingViewRepresentable?
    let onboarding = OnboardingViewModel()
    let viewModel = OnboardingViewModel()
    
    init(provider: OnboardingServiceProvidable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    func onAction(action: OnboardingAction, for screen: OnboardingScreenType) {
        switch action {
        case .inputComplete: validate()
        default: break
        }
    }
    
    private func validate() {
        if email == "" {
            view?.onAction(.requireFields(text: "Enter email addres"))
            return
        }
        
        if !onboarding.validateEmail(email: email) {
            view?.onAction(.errorMessage("Incorrect email"))
            return
        }
        provider.forgot(email: email)
    }
}

extension ForgotPasswordViewModel: OnboardingServiceProvierDelegate, InputViewDelegate {
    func completed<T>(for action: OnboardingAction, with response: T?, with error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.message else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                if let resp = response as? SuccessResponseModel, resp.status != 0 {
                    self.view?.onAction(.errorMessage(resp.message ?? errorMessage))
                } else {
                    self.response = response as? SuccessResponseModel
                    self.view?.onAction(.forgot)
                }
            }
        }
    }
}
