//
//  MyProfileAdaptables.swift
//  Clustry
//
//  Created by cis on 12/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

enum ProfileScreen:String {
    case profile
    case detail
    case edit
    case update
}

enum ProfileButtonAction: String {
    case attachment
    case camera
    case remove
    case save
}

protocol UserImageTableCellDelegate:class {
    func sendAction(action: ProfileButtonAction)
}
protocol MyProfileTableViewDelegate:class {
    func sendAction(action: ProfileButtonAction)
}
protocol AttachmentTableCellDelegate:class {
    func didSelect(action: ProfileButtonAction, index:Int)
}
enum ProfileAction {
    case inputComplete(_ screen:ProfileScreen)
    case editingDidEnd(_ field:String, _ value:String)
    case didTapField(_ field:String)
    case didTapScene(_ scene:Scenes)
    case editingDidChange(_ field:String, _ value:String)
    case requireFields(text:String)
    case validEmail(text:String)
    case errorMessage(_ text:String)
    case successMessage(_ text:String)
    case imageChoosen(_ image:UIImage)
    case chooseAttachment(_ image: [UIImage]?,_ fileURL:URL?, _ name:String?)
    case profile
    case userProfile
    case edit
    case update
    case updateImage
    case profileUpdate(_ text: String)
    case getPrivacy
    case setPrivacy
}

protocol ProfileInputViewDelegate:class {
    func onAction(action: ProfileAction, for screen:ProfileScreen)
}
protocol ProfileViewRepresentable:class {
    func onAction(_ action: ProfileAction)
}
protocol ProfileServiceProviderDelegate:class {
    func completed<T>(for action:ProfileAction, with response:T?, error:APIError?)
}
protocol ProfileServiceProvidable:class {
    var delegate: ProfileServiceProviderDelegate? { get set }
    func profile(param: UserParams.UpdateProfile)
}
