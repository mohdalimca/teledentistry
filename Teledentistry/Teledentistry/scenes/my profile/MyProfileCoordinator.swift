//
//  MyProfileCoordinator.swift
//  Clustry
//
//  Created by cis on 12/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation


final class MyProfileCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: MyProfileViewController = MyProfileViewController.from(from: .main , with: .myProfile)

    var editProfile: EditProfileCoordinator!
    
    override func start() {
        super.start()
        router.setRootModule(controller, hideBar: true)
        self.onStart()
    }
    
    private func onStart() {
//        controller.router = self
    }
    
    private func startEditProfile() {
        let router = Router()
        editProfile = EditProfileCoordinator(router: router)
        add(editProfile)
        editProfile.delegate = self
//        editProfile.start(with: controller.viewModel.user!)
        self.router.present(editProfile, animated: true)
    }
}

extension MyProfileCoordinator: NextSceneDismisser {
    
    func push(scene: Scenes) {
        switch scene {
        case .editProfile: startEditProfile()
        default: break
        }
    }
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension MyProfileCoordinator: CoordinatorDimisser {
    
    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}


