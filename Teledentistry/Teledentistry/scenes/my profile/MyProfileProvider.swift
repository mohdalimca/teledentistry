//
//  MyProfileProvider.swift
//  Clustry
//
//  Created by cis on 12/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

//final class ProfileServiceProvider: ProfileServiceProvidable {
//    
//    var delegate: ProfileServiceProviderDelegate?
//    private var task = UserTask()
//    
//    func profile(param: UserParams.Profile) {
//        task.myProfile(param: param) { [weak self](resp, err) in
//            if err != nil {
//                self?.delegate!.completed(for: .profile, with: resp, error: err)
//                return
//            }
//            self?.delegate!.completed(for: .profile, with: resp, error: err)
//        }
//    }
//    
//    func userProfile(param: UserProfile) {
//        task.userProfile(param: param) { [weak self](resp, err) in
//            if err != nil {
//                self?.delegate!.completed(for: .userProfile, with: resp, error: err)
//                return
//            }
//            self?.delegate!.completed(for: .userProfile, with: resp, error: err)
//        }
//    }
//    
//    func updateProfile(param: ProfileUpdatable) {
//        /*task.updateProfile(param: param) { [weak self](resp, err) in
//            if err != nil {
//                self?.delegate!.completed(for: .update, with: resp, error: err)
//                return
//            }
//            self?.delegate!.completed(for: .update, with: resp, error: err)
//        }*/
//    }
//    
//    func updateUserImage(param: UserParams.ProfileImage) {
//        task.updateProfileImage(param: param) { [weak self](resp, err) in
//            if err != nil {
//                self?.delegate!.completed(for: .updateImage, with: resp, error: err)
//                return
//            }
//            self?.delegate!.completed(for: .updateImage, with: resp, error: err)
//        }
//    }
//    
//    func getUserPrivacy() {
//        task.getUserPrivacy() { [weak self](resp, err) in
//            if err != nil {
//                self?.delegate!.completed(for: .getPrivacy, with: resp, error: err)
//                return
//            }
//            self?.delegate!.completed(for: .getPrivacy, with: resp, error: err)
//        }
//    }
//    
//    func setUserPrivacy(param: UserParams.SetPrivacy) {
//        task.setUserPrivacy(param: param) { [weak self](resp, err) in
//            if err != nil {
//                self?.delegate!.completed(for: .setPrivacy, with: resp, error: err)
//                return
//            }
//            self?.delegate!.completed(for: .setPrivacy, with: resp, error: err)
//        }
//    }
//}
