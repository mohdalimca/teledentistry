//
//  MyProfileViewController.swift
//  Clustry
//
//  Created by cis on 12/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController { }
    
//    @IBOutlet weak var tableView: MyProfileTableView!
//    @IBOutlet weak var lblTitle: UILabel!
//    
//    weak var router: NextSceneDismisser?
//    let viewModel = MyProfileViewModel(provider: ProfileServiceProvider())
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setup()
//    }
//    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        NotificationCenter.default.removeObserver(self)
//    }
//    
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        NotificationCenter.default.removeObserver(self)
//    }
//    
//    private func setup() {
//        tableView.configure(viewModel)
//        tableView.isHidden = true
//        startAnimation()
//        viewModel.view = self
//        viewModel.profile()
//        tableView.didSelectAttachmentIndex = didSelectAttachmentIndex
//        NotificationCenter.default.addObserver(self, selector: #selector(onUserProfileUpdate(_:)), name: .userDataUpdate, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onUserProfileImageUpdate(_:)), name: .userProfileImageUpdate, object: nil)
//    }
//    
//    @objc func onUserProfileUpdate(_ notification:Notification) {
//        viewModel.profile()
//    }
//    
//    @objc func onUserProfileImageUpdate(_ notification:Notification) {
//        viewModel.profile()
//    }
//    
//    private func reload() {
//        if viewModel.user?.attachment != nil {
//            for attachment in (viewModel.user?.attachment!)! {
//                viewModel.selectedAttachmentURL.append(attachment.attachment ?? "")
//            }
//        }
//        tableView.reloadData()
//        tableView.isHidden = false
//        stopAnimating()
//    }
//    
//    private func didSelectAttachmentIndex(_ index:Int) {
//        navigateToImageDetail(index)
//    }
//    
//    private func navigateToImageDetail( _ index:Int) {
//        viewModel.selectedIndex = index
//        router?.push(scene: .imageDetail)
//    }
//
//    @IBAction func btnBackAction(_ sender: UIButton) {
//        router?.dismiss(controller: .myProfile)
//    }
//    
//    @IBAction func btnEditAction(_ sender: UIButton) {
//        router?.push(scene: .editProfile)
//    }
//}
//
//extension MyProfileViewController: ProfileViewRepresentable {
//    func onAction(_ action: ProfileAction) {
//        switch action {
//        case let .errorMessage(text):
//            showSnackBar(text)
//            stopAnimating()
//        case .profile:
//            reload()
//        default:
//            break
//        }
//    }
//}
