//
//  OnboardingAdaptables.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

enum OnboardingScreenType: String {
    case welcome
    case signup
    case login
    case forgot
}
enum OnboardingAction {
    case inputComplete(_ screen: OnboardingScreenType)
    case editingDidEnd(_ field:String, _ value:String)
    case editingDidChange(_ field:String, _ value:String)
    case requireFields(text:String)
    case validEmail(text:String)
    case errorMessage(_ text:String)
    case userRegister
    case userLogin
    case forgot
    case changePassword
    case socialLogin
    case terms
    case privacy
    case feedback
    case aboutUs
    case aboutUsContent
}
protocol InputFieldAlertDelegate:class {
    func userInput(_ text: String)
}
protocol InputViewDelegate:class {
    func onAction(action: OnboardingAction, for screen: OnboardingScreenType)
}
protocol OnboardingViewRepresentable:class {
    func onAction(_ action: OnboardingAction)
}
protocol OnboardingServiceProvidable:class {
    var delegate: OnboardingServiceProvierDelegate? { get set }
    func signup(param:OnboardingParam.Signup)
    func login(email:String, password:String)
    func forgot(email:String)
    func changePassword(oldPassword:String, newPassword:String)
    func socialLogin(param: OnboardingParam.SocialLogin)
    func termsAndCondition()
    func privacyPolicy()
    func aboutUs()
}
protocol OnboardingServiceProvierDelegate:class {
    func completed<T>(for action:OnboardingAction, with response:T?, with error:APIError?)
}
