//
//  LoginCoordinator.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation


final class LoginCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let login: LoginViewController = LoginViewController.from(from: .main, with: .login)
    let forgot: ForgotPasswordViewController = ForgotPasswordViewController.from(from: .main, with: .forgot)
    var welcome: Bool!
    var signup: SignupCoordinator!

    override func start() {
        super.start()
        router.setRootModule(login, hideBar: true)
        self.onStart()
    }
    
    func start(with welcome: Bool) {
        super.start()
//        login.welcome = welcome
        self.welcome = welcome
        router.setRootModule(login, hideBar: true)
        self.onStart()
    }
    
    private func onStart() {
//        login.router = self
//        forgot.router = self
    }
    
    private func startSignup() {
        let router = Router()
        signup = SignupCoordinator(router: router)
        add(signup)
        signup.delegate = self
        signup.start(with: !self.welcome)
        self.router.present(signup, animated: true)
    }
    
}

extension LoginCoordinator: NextSceneDismisser {
    
    func push(scene: Scenes) {
        switch scene {
        case .forgot: router.present(forgot, animated: true)
        case .signup: startSignup()
        default: break
        }
        
    }
    
    func dismiss(controller: Scenes) {
        if controller == .forgot {
            router.dismissModule(animated: true, completion: nil)
        } else {
            delegate?.dismiss(coordinator: self)
        }
    }
}

extension LoginCoordinator: CoordinatorDimisser {

    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}

