//
//  LoginViewController.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit
import GoogleSignIn
import AuthenticationServices
import Firebase

class LoginViewController: UIViewController {}

//class LoginViewController: UIViewController, GIDSignInDelegate {
//    
//    @IBOutlet weak var btnBack: UIButton!
//    @IBOutlet weak var btnForgot: UIButton!
//    @IBOutlet weak var btnSignup: UIButton!
//    @IBOutlet weak var btnLogin: UIButton!
//    @IBOutlet weak var btnApple: UIButton!
//    @IBOutlet weak var btnFacebook: UIButton!
//    @IBOutlet weak var btnGoogle: UIButton!
//    @IBOutlet weak var btnPasswordEye: UIButton!
//    
//    @IBOutlet weak var txtEmail: UITextField!
//    @IBOutlet weak var txtPassword: UITextField!
//    
//    @IBOutlet weak var lblLogin: UILabel!
//    @IBOutlet weak var lblEmail: UILabel!
//    @IBOutlet weak var lblPassword: UILabel!
//    
//    @IBOutlet weak var viewEmail: UIView!
//    @IBOutlet weak var viewPassword: UIView!
//    @IBOutlet weak var viewLogin: UIView!
//    @IBOutlet weak var viewApple: UIView!
//    @IBOutlet weak var viewGoogle: UIView!
//    @IBOutlet weak var viewFacebook: UIView!
//    
//    @IBOutlet weak var stackView: UIStackView!
//    
//    weak var router: NextSceneDismisser?
//    private let viewModel = LoginViewModel(provider: OnboardingServiceProvider())
//    var welcome: Bool!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setup()
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//    }
//    
//    private func setup() {
//        viewModel.view = self
//        
//        [btnBack, btnForgot, btnSignup, btnApple, btnFacebook, btnGoogle, btnLogin, btnPasswordEye].forEach {
//            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
//        }
//        [txtEmail, txtPassword].forEach {
//            $0?.delegate = self
//            $0?.addTarget(self, action: #selector(textFieldDidChanged(textField:)), for: .editingChanged)
//        }
//        [viewEmail, viewPassword].forEach {
//            $0?.borderColor = .lightGray
//        }
//        
//        [viewLogin, viewApple, viewFacebook, viewGoogle].forEach {
//            $0?.layer.cornerRadius = ($0?.frame.size.height)! / 2
//        }
//        
//        
//        // Apple
//        self.setAppleButton()
//        
//        //Google
//        GIDSignIn.sharedInstance()?.presentingViewController = self
//        //        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
//    }
//    
//    private func setAppleButton() {
//        if #available(iOS 13.0, *) {
//            viewApple.isHidden = false
//            let btn = ASAuthorizationAppleIDButton(authorizationButtonType: .continue, authorizationButtonStyle: .black)
//            btn.frame = viewApple.bounds
//            btn.addTarget(self, action: #selector(self.actionSignInwithApple), for: .touchUpInside)
//            self.stackView.addArrangedSubview(btn)
//        } else {
//            viewApple.isHidden = true
//        }
//    }
//    
//    @objc func actionSignInwithApple() {
//        if #available(iOS 13.0, *) {
//            let appleIDProvider = ASAuthorizationAppleIDProvider()
//            let request = appleIDProvider.createRequest()
//            request.requestedScopes = [.fullName, .email]
//            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//            authorizationController.delegate = self
//            authorizationController.presentationContextProvider = self
//            authorizationController.performRequests()
//        } else {
//            // Fallback on earlier versions
//        }
//    }
//    
//    
//    @objc func buttonPressed(_ sender: UIButton) {
//        self.view.endEditing(true)
//        switch sender {
//        case btnBack:
//            router?.dismiss(controller: .login)
//        case btnForgot:
//            router?.push(scene: .forgot)
//        case btnSignup:
//            welcome ? router?.push(scene: .signup) : router?.dismiss(controller: .signup)
//        case btnLogin:
//            startAnimation()
//            viewModel.onAction(action: .inputComplete(.login), for: .login)
//        case btnApple:
//            appleLogin()
//        case btnFacebook:
//            break
//        case btnGoogle:
//            googleLogin()
//        case btnPasswordEye:
//            txtPassword.isSecureTextEntry = btnPasswordEye.isSelected
//            btnPasswordEye.isSelected = !btnPasswordEye.isSelected
//        default:
//            break
//        }
//    }
//    
//    private func socialLogin() {
//        self.startAnimation()
////        self.viewModel.socialLogin(param: UserParams.SocialLogin(username: self.viewModel.name, email: self.viewModel.email, social_id: self.viewModel.socialId, type: self.viewModel.loginType, isverified: self.viewModel.isVerified, device_id: UserStore.pushToken ?? "", device_type: device_type))
//    }
//    
//    private func googleLogin() {
//        GIDSignIn.sharedInstance()?.delegate = self
//        GIDSignIn.sharedInstance()?.presentingViewController = self
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
//        GIDSignIn.signIn(GIDSignIn.sharedInstance())()
//        GIDSignIn.signOut(GIDSignIn.sharedInstance())()
//    }
//    
////    private func setSocialLoginValues(email:String, name:String, socialId:String, loginType:SocialLoginType)  {
////        self.viewModel.email = email
////        self.viewModel.name = name
////        self.viewModel.socialId = socialId
////        self.viewModel.isVerified = false
////        self.viewModel.loginType = loginType
////        if self.viewModel.email != "" && self.viewModel.email != nil {
////            self.viewModel.isVerified = true
////        }
////
////        if self.viewModel.isVerified {
////            self.socialLogin()
////        } else {
////            self.showInputAlert(controller: self, type: .email)
////        }
////    }
//    
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//        if let error = error {
//            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
//                //                showSnackBar("The user has not signed in before or they have since signed out.")
//                print("The user has not signed in before or they have since signed out.")
//            } else {
//                print("\(error.localizedDescription)")
//            }
//            return
//        }
////        self.setSocialLoginValues(email: user.profile.email, name: user.profile.name, socialId: user.userID, loginType: .google)
//    }
//    
//    
//    private func appleLogin() {
//        if #available(iOS 13.0, *) {
//            let appleProvider = ASAuthorizationAppleIDProvider()
//            let request = appleProvider.createRequest()
//            request.requestedScopes = [ .fullName, .email]
//            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//            authorizationController.delegate = self
//            authorizationController.performRequests()
//        } else {
//            
//        }
//    }
//    
//    
//    
//    private func signInOnFirebase() {
//        //        Auth.auth().signIn(withEmail: viewModel.email!, password: viewModel.password) { [weak self](result, error) in
//        //            if error == nil {
//        //            }
//        //        }
//    }
//    
//    private func navigateToNextScreen() {
//        //        stopAnimating()
//        //        router?.push(scene: .category)
//        stopAnimating()
//        if UserStore.firstLaunch != nil {
////            router?.push(scene: .home)
//        } else {
//            UserStore.save(firstLaunch: true)
////            router?.push(scene: .walkthrough)
//        }
//    }
//}
//
//
//extension LoginViewController: UITextFieldDelegate {
//    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == txtEmail {
//            viewEmail.borderColor = .orange
//        } else {
//            viewPassword.borderColor = .orange
//        }
//    }
//    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == txtEmail {
//            viewEmail.borderColor = .lightGray
//            viewModel.email = textField.text!
//        } else {
//            viewPassword.borderColor = .lightGray
//            viewModel.password = textField.text!
//        }
//    }
//    
//    @objc func textFieldDidChanged(textField: UITextField) {
//        switch textField {
//        case txtEmail:
//            viewModel.email = textField.text!
//        case txtPassword:
//            viewModel.password = textField.text!
//        default:break
//        }
//    }
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if let _ = string.rangeOfCharacter(from: .whitespacesAndNewlines) {
//            return false
//        }
//        return true
//    }
//}
//
//
//extension LoginViewController: InputFieldAlertDelegate {
//    func userInput(_ text: String) {
//        self.viewModel.email = text
//        self.socialLogin()
//    }
//}
//
//extension LoginViewController: ASAuthorizationControllerDelegate {
//    @available(iOS 13.0, *)
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
//        if let appleCredentials = authorization.credential as? ASAuthorizationAppleIDCredential {
////            self.setSocialLoginValues(email: appleCredentials.email!, name: (appleCredentials.fullName?.givenName)!, socialId: appleCredentials.user, loginType: .apple)
//        }
//    }
//    
//    @available(iOS 13.0, *)
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
//        print(error.localizedDescription)
//    }
//}
//
//
//extension LoginViewController: ASAuthorizationControllerPresentationContextProviding {
//    //For present window
//    @available(iOS 13.0, *)
//    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
//        return self.view.window!
//    }
//}
//
//extension LoginViewController: OnboardingViewRepresentable {
//    func onAction(_ action: OnboardingAction) {
//        switch action {
//        case let .errorMessage(text), let .requireFields(text: text):
//            stopAnimating()
//            showSnackBar(text)
//        case .userLogin, .socialLogin:
//            navigateToNextScreen()
//        default:
//            break
//        }
//    }
//}
//
