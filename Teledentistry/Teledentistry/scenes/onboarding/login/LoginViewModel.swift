//
//  LoginViewModel.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

enum SocialLoginType: String, Codable {
    case facebook = "Facebook"
    case google = "Google"
    case apple = "Apple"
}


final class LoginViewModel {
    
    var email: String?
    var password = ""
    var socialId = ""
    var isVerified = false
    var name = ""
    var loginType: SocialLoginType?
    
    var response: SuccessResponseModel?
    weak var view: OnboardingViewRepresentable?
    let onboarding = OnboardingViewModel()
    let provider: OnboardingServiceProvidable
    
    init(provider: OnboardingServiceProvidable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    func onAction(action: OnboardingAction, for screen: OnboardingScreenType) {
        switch action {
        case .inputComplete: valiadate()
        default: break
        }
    }
    
    private func valiadate() {
        if email == nil {
            view?.onAction(.requireFields(text: "Email is required"))
            print("Email is required")
            return
        }
        if !onboarding.validateEmail(email: email!) {
            view?.onAction(.requireFields(text: "Invalid email"))
            print("Invalid email")
            return
        }
        if password == "" {
            view?.onAction(.requireFields(text: "Password is required"))
            print("Password is required")
            return
        }
        provider.login(email: email!, password: password)
    }
    
    func socialLogin(param: OnboardingParam.SocialLogin) {
//        provider.socialLogin(param: param)
    }
}



extension LoginViewModel: OnboardingServiceProvierDelegate, InputViewDelegate {
    
    func completed<T>(for action: OnboardingAction, with response: T?, with error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.message else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                if let resp = response as? SuccessResponseModel {
                    if resp.status != 0 {
//                        UserStore.save(password: resp.data?.user?.originalPassword ?? "")
//                        UserStore.save(username: resp.data?.user?.username ?? "")
//                        UserStore.save(userImage: resp.data?.user?.image ?? "")
//                        UserStore.save(email: resp.data?.user?.email ?? "")
                        self.view?.onAction(action)
                    } else {
                        self.view?.onAction(.errorMessage(resp.message ?? errorMessage))
                    }
                } else {
                    self.view?.onAction(.errorMessage((response as? SuccessResponseModel)?.message ?? errorMessage))
                }
            }
        }
    }
}
