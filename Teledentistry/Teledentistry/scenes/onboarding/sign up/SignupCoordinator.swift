//
//  SignupCoordinator.swift
//  Clustry
//
//  Created by cis on 06/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation


final class SignupCoordinator: Coordinator<Scenes> {
    
    weak var delegate: CoordinatorDimisser?
    let controller: SignupViewController = SignupViewController.from(from: .main, with: .signup)
    var welcome: Bool!
    var login: LoginCoordinator!
    
    override func start() {
        super.start()
        router.setRootModule(controller, hideBar: true)
        self.onStart()
    }
    
    func start(with welcome: Bool) {
        super.start()
        self.welcome = welcome
        router.setRootModule(controller, hideBar: true)
        self.onStart()
    }
    
    private func onStart() {
//        controller.router = self
    }
    
    private func startLogin() {
        let router = Router()
        login = LoginCoordinator(router: router)
        add(login)
        login.delegate = self
        login.start(with: !self.welcome)
        self.router.present(login, animated: true)
    }
}

extension SignupCoordinator: NextSceneDismisser {
    
    func push(scene: Scenes) {
        switch scene {
        case .login: startLogin()
        case .signup: break
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {
        delegate?.dismiss(coordinator: self)
    }
}

extension SignupCoordinator: CoordinatorDimisser {

    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
