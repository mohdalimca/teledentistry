//
//  SignupViewController.swift
//  Clustry
//
//  Created by cis on 06/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit
import MediaAccessibility


class SignupViewController: UIViewController { }
//    
//    @IBOutlet weak var btnBack: UIButton!
//    @IBOutlet weak var btnAccept: UIButton!
//    @IBOutlet weak var btnSignup: UIButton!
//    @IBOutlet weak var btnLogin: UIButton!
//    @IBOutlet weak var btnPasswordEye: UIButton!
//    @IBOutlet weak var btnConfirmEye: UIButton!
//    @IBOutlet weak var btnCountryCode: UIButton!
//    @IBOutlet weak var txtEmail: UITextField!
//    @IBOutlet weak var txtPhoneNo: UITextField!
//    @IBOutlet weak var txtUsername: UITextField!
//    @IBOutlet weak var txtPassword: UITextField!
//    @IBOutlet weak var txtConfirmPassword: UITextField!
//    @IBOutlet weak var viewUsername: UIView!
//    @IBOutlet weak var viewEmail: UIView!
//    @IBOutlet weak var viewPhoneNo: UIView!
//    @IBOutlet weak var viewPassword: UIView!
//    @IBOutlet weak var viewConfirmPassword: UIView!
//    
//    var jsonArray: [Any] = []
//    var arrCountry: [String] = []
//    let picker: PickerView = PickerView.from(from: .main, with: .pickerView)
//    let viewModel = SignupViewModel(provider: OnboardingServiceProvider())
//    weak var router: NextSceneDismisser?
//    var welcome: Bool!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setup()
//    }
//    
//    private func setup() {
//        let myNormalAttributedTitle = NSAttributedString(string: "Login",
//                                                         attributes: [NSAttributedString.Key.foregroundColor : UIColor.orange])
//        btnLogin.setAttributedTitle(myNormalAttributedTitle, for: .normal)
//
//        [btnAccept, btnBack, btnLogin, btnSignup, btnCountryCode, btnPasswordEye, btnConfirmEye].forEach {
//            $0?.layer.cornerRadius = ($0?.frame.size.height)! / 2
//            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
//        }
//        [txtUsername, txtEmail, txtPhoneNo, txtPassword, txtConfirmPassword].forEach {
//            $0?.delegate = self
//            $0?.addTarget(self, action: #selector(textFieldDidChanged(textField:)), for: .editingChanged)
//        }
//        [viewUsername, viewEmail, viewPhoneNo, viewPassword, viewConfirmPassword].forEach {
//            $0?.borderColor = .lightGray
//        }
//        
//        countryList.delegate = self
//        viewModel.view = self
//        setFonts()
//    }
//    
//    private func loadCountryJsonList() {
//        guard let path = Bundle.main.path(forResource: "country_list", ofType:"json") else {
//            debugPrint( "path not found")
//            return
//        }
//        do{
//            let data = try Data(contentsOf: URL(fileURLWithPath: path))
//            let json = try JSONSerialization.jsonObject(with: data, options: [])
//            if let object = json as? [String : Any]  {
//                print(object)
//            }
//            else if let Anyjson = json as? [Any] {
//                jsonArray = Anyjson
//                picker.data.removeAll()
//                for index in 0..<jsonArray.count {
//                    let dict = jsonArray[index] as? NSDictionary
//                    let code = (dict?.object(forKey: "dial_code") as? String ?? "") + " " + (dict?.object(forKey: "name") as? String ?? "")
//                    picker.data.append(code)
//                }
//                //                dictItem.object(forKey: "dial_code") ?? ""
//                //                self?.onResult( "\(dictItem.object(forKey: "dial_code") ?? "")")
//                
//            } else {
//                print("error in formating")
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//    }
//    
//    private func setFonts() {
//        if iOSDevice == .iphone {
//            setATtributedFont(14)
//        } else {
//            setATtributedFont(24)
//        }
//    }
//    
//    private func setATtributedFont(_ size:CGFloat) {
//        let myNormalAttributedTitle = NSAttributedString(string: "Login",
//                                                         attributes: [NSAttributedString.Key.foregroundColor : UIColor.init(cgColor: #colorLiteral(red: 1, green: 0.4840375185, blue: 0, alpha: 1)), NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])
//        btnLogin.titleLabel?.font = UIFont(name: AppFonts.RobotoSlabBold.rawValue, size: size)
//        btnLogin.setAttributedTitle(myNormalAttributedTitle, for: .normal)
//    }
//    
//    @objc func buttonPressed(_ sender: UIButton) {
//        self.view.endEditing(true)
//        switch sender {
//        case btnBack:
//            router?.dismiss(controller: .signup)
//        case btnAccept:
//            if !btnAccept.isSelected {
//                btnAccept.isSelected = true
//            } else {
//                btnAccept.isSelected = false
//            }
//            //            viewModel.isTermsAccepted = btnAccept.isSelected == (!btnAccept.isSelected) ? true : false
//            viewModel.isTermsAccepted = btnAccept.isSelected
//        case btnSignup:
//            startAnimation()
//            viewModel.onAction(action: .inputComplete(.signup), for: .signup)
//        case btnLogin:
//            welcome ? router?.push(scene: .login) : router?.dismiss(controller: .login)
//        case btnCountryCode:
//            //            showPicker()
//            present(countryList, animated: true, completion: nil)
//            
//            //            let obj = DropDownView()
//            //            obj.show(title: "Select Country", arr: jsonArray) { [weak self](strCountryCode) in
//            //                self?.btnCountryCode.setTitle(strCountryCode, for: .normal)
//        //            }
//        case btnPasswordEye:
//            txtPassword.isSecureTextEntry = btnPasswordEye.isSelected
//            btnPasswordEye.isSelected = !btnPasswordEye.isSelected
//        case btnConfirmEye:
//            txtConfirmPassword.isSecureTextEntry = btnConfirmEye.isSelected
//            btnConfirmEye.isSelected = !btnConfirmEye.isSelected
//        default:
//            break
//        }
//    }
//    
//    private func showPicker() {
//        picker.delegate = self
//        picker.selectedValue = ""
//        picker.modalTransitionStyle = .crossDissolve
//        picker.modalPresentationStyle = .overFullScreen
//        self.present(picker, animated: true, completion: nil)
//    }
//    
//    private func authWithFirebase() {
////        Auth.auth().createUser(withEmail: viewModel.email, password: viewModel.password) { [weak self] (result, error) in
////            if error == nil {
////                print("User created on firebase")
////            }
////        }
//    }
//    
//    private func navigateToLogin() {
//        stopAnimating()
//        
//        showSnackBar((viewModel.response?.message)!)
//        router?.push(scene: .login)
//    }
//    
//    @objc func textFieldDidChanged(textField: UITextField) {
//        switch textField {
//        case txtUsername:
//            viewModel.username = textField.text!
//        case txtEmail:
//            viewModel.email = textField.text!
//        case txtPhoneNo:
//            viewModel.phoneNumber = textField.text!
//        case txtPassword:
//            viewModel.password = textField.text!
//        case txtConfirmPassword:
//            viewModel.confirmPassword = textField.text!
//        default:break
//        }
//    }  
//}
//
//extension SignupViewController: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        switch textField {
//        case txtUsername:
//            viewUsername.borderColor = .orange
//            viewModel.username = textField.text!
//        case txtEmail:
//            viewEmail.borderColor = .orange
//            viewModel.email = textField.text!
//        case txtPhoneNo:
//            viewPhoneNo.borderColor = .orange
//            viewModel.phoneNumber = textField.text!
//        case txtPassword:
//            viewPassword.borderColor = .orange
//            viewModel.password = textField.text!
//        case txtConfirmPassword:
//            viewConfirmPassword.borderColor = .orange
//            viewModel.confirmPassword = textField.text!
//        default:break
//        }
//    }
//    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        switch textField {
//        case txtUsername:
//            viewUsername.borderColor = .lightGray
//            viewModel.username = textField.text!
//        case txtEmail:
//            viewEmail.borderColor = .lightGray
//            viewModel.email = textField.text!
//        case txtPhoneNo:
//            viewPhoneNo.borderColor = .lightGray
//            viewModel.phoneNumber = textField.text!
//        case txtPassword:
//            viewPassword.borderColor = .lightGray
//            viewModel.password = textField.text!
//        case txtConfirmPassword:
//            viewConfirmPassword.borderColor = .lightGray
//            viewModel.confirmPassword = textField.text!
//        default:break
//        }
//    }
//    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == txtPhoneNo {
//            let MAX_LENGTH_PHONENUMBER = 10
//            let ACCEPTABLE_NUMBERS = "0123456789"
//            let newLength: Int = textField.text!.count + string.count - range.length
//            let numberOnly = NSCharacterSet.init(charactersIn: ACCEPTABLE_NUMBERS).inverted
//            let strValid = string.rangeOfCharacter(from: numberOnly) == nil
//            return (strValid && (newLength <= MAX_LENGTH_PHONENUMBER))
//        }
//        if textField == txtUsername {  return true }
//        if let _ = string.rangeOfCharacter(from: .whitespacesAndNewlines) { return false }
//        return true
//    }
//}
//
//extension SignupViewController: CountryListDelegate {
//    func selectedCountry(country: Country) {
//        viewModel.selectedCountry = country
//        updateCountryCode()
//    }
//}
//
//extension SignupViewController: OnboardingViewRepresentable {
//    func onAction(_ action: OnboardingAction) {
//        switch action {
//        case let .errorMessage(text), let .requireFields(text: text):
//            stopAnimating()
//            showSnackBar(text)
//        case .userRegister:
//            navigateToLogin()
//        default: break
//        }
//    }
//}
//
//extension SignupViewController: GetPickerValueDelagate {
//    func getValue(_ value: String, index: Int, scene: Scenes?) {
//    }
//}
//
