//
//  SignupViewModel.swift
//  Clustry
//
//  Created by cis on 14/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

final class SignupViewModel {
    
    var username = ""
    var email = ""
    var phoneNumber = ""
    var password = ""
    var confirmPassword = ""
    var isTermsAccepted = false
    var countryCode:String = ""
    
    var response: SuccessResponseModel?
    weak var view: OnboardingViewRepresentable?
    let onboarding = OnboardingViewModel()
    let provider: OnboardingServiceProvidable
    
    init(provider: OnboardingServiceProvidable) {
        self.provider = provider
        self.provider.delegate = self
    }
    
    func onAction(action: OnboardingAction, for screen: OnboardingScreenType) {
        switch action {
        case .inputComplete: validate()
        default: break
        }
    }
    
    private func validate() {
//        if username == "" {
//            print("Username is required")
//            view?.onAction(.requireFields(text: "Username is required"))
//            return
//        }
        if email == "" {
            view?.onAction(.requireFields(text: "Email is required"))
            print("Email is required")
            return
        }
        if !onboarding.validateEmail(email: email) {
            view?.onAction(.requireFields(text: "Invalid email"))
            print("Invalid email")
            return
        }
//        if phoneNumber == "" {
//            view?.onAction(.requireFields(text: "Phone number is required"))
//            print("Phone number is required")
//            return
//        }
//        if !onboarding.validatePhone(phone: phoneNumber){
//            view?.onAction(.requireFields(text: "Invalid Phone Number"))
//            print("Invalid Phone Number")
//            return
//        }
        
        if password == "" {
            view?.onAction(.requireFields(text: "Password is required"))
            print("Password is required")
            return
        }
        
        if confirmPassword == "" {
            view?.onAction(.requireFields(text: "Confirm Password is required"))
            print("Password is required")
            return
        }
        
        if confirmPassword.trimmingCharacters(in: .whitespaces) != password.trimmingCharacters(in: .whitespaces) {
            view?.onAction(.requireFields(text: "Password does not match"))
            print("Password is required")
            return
        }
        
        if !onboarding.validatePassword(password: password){
            view?.onAction(.requireFields(text: "Password length should be 8 character"))
            return
        }
        
        if !isTermsAccepted {
            view?.onAction(.requireFields(text: "Please aceept the terms and condition to proceed"))
            return
        }
//        provider.signup(param:UserParams.Signup(username: username, email: email, password: password, phone: phoneNumber, device_id: UserStore.pushToken ?? "", device_type: device_type, isd_code:selectedCountry.phoneExtension))
    }
}

extension SignupViewModel: OnboardingServiceProvierDelegate, InputViewDelegate {
    func completed<T>(for action: OnboardingAction, with response: T?, with error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                guard let message = error?.responseData?.message else {
                    self.view?.onAction(.errorMessage(errorMessage))
                    return
                }
                self.view?.onAction(.errorMessage(message))
            } else {
                if let resp = response as? SuccessResponseModel, resp.status != 0 {
                    self.view?.onAction(.errorMessage(resp.message ?? errorMessage))
                } else {
                    self.response = response as? SuccessResponseModel
                    self.view?.onAction(.userRegister)
                }
            }
        }
    }
}
