//
//  WelcomCoordinator.swift
//  Clustry
//
//  Created by cis on 06/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

final class WelcomeCoordinator: Coordinator<Scenes> {
    
    let welcome: WelcomeViewController = WelcomeViewController.from(from: .main, with: .welcome)
    
    var signup: SignupCoordinator!
    var login: LoginCoordinator!
    
    override func start() {
        super.start()
        router.setRootModule(welcome, hideBar: true)
        self.onStart()
    }
    
    private func onStart() {
//        welcome.router = self
    }
    
    private func startSignup() {
        let router = Router()
        signup = SignupCoordinator(router: router)
        add(signup)
        signup.delegate = self
        signup.start(with: true)
        self.router.present(signup, animated: true)
    }
    
    private func startLogin() {
        let router = Router()
        login = LoginCoordinator(router: router)
        add(login)
        login.delegate = self
        login.start(with: true)
        self.router.present(login, animated: true)
    }
    
}

extension WelcomeCoordinator: NextSceneDismisser {
    
    func push(scene: Scenes) {
        switch scene {
        case .login: startLogin()
        case .signup: startSignup()
        default: break
        }
    }
    
    func dismiss(controller: Scenes) {}
}

extension WelcomeCoordinator: CoordinatorDimisser {

    func dismiss(coordinator: Coordinator<Scenes>) {
        remove(child: coordinator)
        router.dismissModule(animated: true, completion: nil)
    }
}
