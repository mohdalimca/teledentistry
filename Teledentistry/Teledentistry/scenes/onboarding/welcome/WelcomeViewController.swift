//
//  WelcomViewController.swift
//  Clustry
//
//  Created by cis on 06/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit
import GoogleSignIn
import AuthenticationServices

class WelcomeViewController: UIViewController {}

//class WelcomeViewController: UIViewController,  GIDSignInDelegate {
//    
//    @IBOutlet weak var lblAppTitle: UILabel!
//    @IBOutlet weak var lblAppSubtitle: UILabel!
//    @IBOutlet weak var lblSignupTitle: UILabel!
//    @IBOutlet weak var lblSignupSubtitle: UILabel!
//    
//    @IBOutlet weak var btnEmail: UIButton!
//    @IBOutlet weak var btnApple: UIButton!
//    @IBOutlet weak var btnFacebook: UIButton!
//    @IBOutlet weak var btnGmail: UIButton!
//    @IBOutlet weak var btnLogin: UIButton!
//    
//    @IBOutlet weak var viewFront: UIView!
//    @IBOutlet weak var viewEmail: UIView!
//    @IBOutlet weak var viewApple: UIView!
//    @IBOutlet weak var viewGoogle: UIView!
//    @IBOutlet weak var viewFacebook: UIView!
//    
//    @IBOutlet weak var stackView: UIStackView!
//    
//    @IBOutlet weak var cnsFrontViewCenterY: NSLayoutConstraint!
//    
//    weak var router: NextSceneDismisser?
//    private let viewModel = LoginViewModel(provider: OnboardingServiceProvider())
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setup()
//    }
//    
//    private func setup() {
//        animateLogoView()
//        viewModel.view = self
//        GIDSignIn.sharedInstance()?.presentingViewController = self
////        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
//        [btnEmail, btnApple, btnFacebook, btnGmail, btnLogin].forEach {
//            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
//        }
//        
//        [viewEmail, viewApple, viewFacebook, viewGoogle].forEach {
//            $0?.layer.cornerRadius = ($0?.frame.size.height)! / 2
//            $0?.layer.masksToBounds = true
//        }
//        setAppleButton()
//    }
//    
//    private func setAppleButton() {
//        if #available(iOS 13.0, *) {
//            viewApple.isHidden = false
//            let btn = ASAuthorizationAppleIDButton(authorizationButtonType: .continue, authorizationButtonStyle: .black)
//            btn.frame = viewApple.bounds
//            btn.addTarget(self, action: #selector(self.actionSignInwithApple), for: .touchUpInside)
//            self.stackView.addArrangedSubview(btn)
//        } else {
//            viewApple.isHidden = true
//        }
//    }
//    
//    @objc func actionSignInwithApple() {
//        if #available(iOS 13.0, *) {
//            let appleIDProvider = ASAuthorizationAppleIDProvider()
//            let request = appleIDProvider.createRequest()
//            request.requestedScopes = [.fullName, .email]
//            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//            authorizationController.delegate = self
//            authorizationController.presentationContextProvider = self
//            authorizationController.performRequests()
//        } else {
//            // Fallback on earlier versions
//        }
//    }
//    
//    
//    private func animateLogoView() {
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
//            UIView.animate(withDuration: 0.3, animations: {
//                let newConstraint = self.cnsFrontViewCenterY.constraintWithMultiplier(0.4)
//                self.viewFront.removeConstraint(self.cnsFrontViewCenterY)
//                self.viewFront.addConstraint(newConstraint)
//                self.viewFront.layoutIfNeeded()
//                self.cnsFrontViewCenterY = newConstraint
//            }) { (isCompleted) in
//                if isCompleted{
//                    self.viewFront.alpha = 0.0
//                }
//            }
//        }
//    }
//    
//    @objc func buttonPressed(_ sender: UIButton) {
//        switch sender {
//        case btnEmail:
//            router?.push(scene: .signup)
//        case btnApple:
//            appleLogin()
//        case btnFacebook:
//            break
//        case btnGmail:
//            btnGoogleAction()
//        case btnLogin:
//            router?.push(scene: .login)
//        default:
//            break
//        }
//    }
//    
//    private func socialLogin() {
//        self.startAnimation()
////        self.viewModel.socialLogin(param: UserParams.SocialLogin(username: self.viewModel.name, email: self.viewModel.email, social_id: self.viewModel.socialId, type: self.viewModel.loginType, isverified: self.viewModel.isVerified, device_id: UserStore.pushToken ?? "", device_type: device_type))
//    }
//    
//    
////    private func setSocialLoginValues(email:String, name:String, socialId:String, loginType:SocialLoginType)  {
////        self.viewModel.email = email
////        self.viewModel.name = name
////        self.viewModel.socialId = socialId
////        self.viewModel.isVerified = false
////        self.viewModel.loginType = loginType
////        if self.viewModel.email != "" && self.viewModel.email != nil {
////            self.viewModel.isVerified = true
////        }
////
////        if self.viewModel.isVerified {
////            self.socialLogin()
////        } else {
////            self.showInputAlert(controller: self, type: .email)
////        }
////    }
//    
//    private func btnGoogleAction() {
//        GIDSignIn.sharedInstance()?.delegate = self
//        GIDSignIn.sharedInstance()?.presentingViewController = self
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
//        GIDSignIn.signIn(GIDSignIn.sharedInstance())()
//        GIDSignIn.signOut(GIDSignIn.sharedInstance())()
//    }
//    
//    // MARK:- Google Delegate
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
//        if let error = error {
//            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
//                //                showSnackBar("The user has not signed in before or they have since signed out.")
//                print("The user has not signed in before or they have since signed out.")
//            } else {
//                print("\(error.localizedDescription)")
//            }
//            return
//        }
////        self.setSocialLoginValues(email: user.profile.email, name: user.profile.name, socialId: user.userID, loginType: .google)
//    }
//    
//    private func appleLogin() {
//        if #available(iOS 13.0, *) {
//            let appleProvider = ASAuthorizationAppleIDProvider()
//            let request = appleProvider.createRequest()
//            request.requestedScopes = [ .fullName, .email]
//            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
//            authorizationController.delegate = self
//            authorizationController.performRequests()
//        } else {
//            // Fallback on earlier versions
//        }
//    }
//    
//    private func signInOnFirebase() {   }
//        
//    private func navigateToNextScreen() {
//        stopAnimating()
////        if UserStore.firstLaunch != nil {
////            router?.push(scene: .home)
////        } else {
////            UserStore.save(firstLaunch: true)
////            router?.push(scene: .walkthrough)
////        }
//    }
//}
//
//
//extension WelcomeViewController: InputFieldAlertDelegate {
//    func userInput(_ text: String) {
//        self.viewModel.email = text
//        self.socialLogin()
//    }
//}
//
//extension WelcomeViewController: ASAuthorizationControllerDelegate {
//    @available(iOS 13.0, *)
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
//        if let appleCredentials = authorization.credential as? ASAuthorizationAppleIDCredential {
////            self.setSocialLoginValues(email: appleCredentials.email!, name: (appleCredentials.fullName?.givenName)!, socialId: appleCredentials.user, loginType: .apple)
//        }
//    }
//    
//    @available(iOS 13.0, *)
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
//        print(error.localizedDescription)
//    }
//}
//
//
//extension WelcomeViewController: ASAuthorizationControllerPresentationContextProviding {
//    //For present window
//    @available(iOS 13.0, *)
//    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
//        return self.view.window!
//    }
//}
//
//extension WelcomeViewController: OnboardingViewRepresentable {
//    func onAction(_ action: OnboardingAction) {
//        switch action {
//        case let .errorMessage(text):
//            showSnackBar(text)
//            stopAnimating()
//        case .socialLogin:
//            navigateToNextScreen()
//        default:
//            break
//        }
//    }
//}
//
