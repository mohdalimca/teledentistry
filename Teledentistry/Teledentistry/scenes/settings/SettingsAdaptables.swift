//
//  SettingsAdaptables.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

protocol SettingsTableDelegate: class {
    func didSelect(action: Scenes)
}

protocol SettingsHeaderViewDelegate: class {
    func sendAction(action: Scenes)
}
