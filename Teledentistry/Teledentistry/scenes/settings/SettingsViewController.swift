
//
//  SettingsViewController.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController { }
    
//    @IBOutlet weak var headerView: UIView!
//    @IBOutlet weak var tableView: SettingsTableView!
//    @IBOutlet weak var userInfoView: SettingsHeaderView!
//    
//    weak var router: NextSceneDismisser?
//    var landingHeader: LandingHeaderView?
//    private let viewModel = SettingsViewModel(provider: ProfileServiceProvider())
//    weak var tableDelegate: SettingsTableDelegate?
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setup()
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        startAnimation()
//        viewModel.view = self
//        viewModel.profile()
//    }
//    
//    private func setup() {
//        tableView.tableDelegate = tableDelegate
//        tableView.configure(viewModel)
//        userInfoView.delegate = self
//        setHeader()
//        NotificationCenter.default.addObserver(self, selector: #selector(onUserProfileUpdate(_:)), name: .userDataUpdate, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(onUserProfileImageUpdate(_:)), name: .userProfileImageUpdate, object: nil)
//    }
//
//    @objc func onUserProfileImageUpdate(_ notification:Notification) {
//        viewModel.profile()
//    }
//    
//    @objc func onUserProfileUpdate(_ notification:Notification) {
//        viewModel.profile()
//    }
//    
//    
//    private func setHeader() {
//        landingHeader = Bundle.main.loadNibNamed("LandingHeaderView", owner: nil, options: nil)![0] as? LandingHeaderView
//        landingHeader?.frame = CGRect.init(x: 0, y: 0, width: headerView.bounds.width, height: headerView.bounds.height)
//        landingHeader?.landingScreen = .settings
//        landingHeader?.lblTitle.text = LandingScreen.settings.rawValue.capitalized
//        headerView.addSubview(landingHeader!)
//    }
//    
//    private func populateDetail() {
//        userInfoView.configureDetail(user: viewModel.user!)
//        userInfoView.configureDetail(name:viewModel.user?.firstName ?? "", email: viewModel.user?.email ?? "")
//        stopAnimating()
//    }
//}
//
//extension SettingsViewController: SettingsHeaderViewDelegate {
//    func sendAction(action: Scenes) {
//        router?.push(scene: .myProfile)
//    }
//}
//
//extension SettingsViewController: ProfileViewRepresentable {
//    func onAction(_ action: ProfileAction) {
//        switch action {
//        case let .errorMessage(text):
//            showSnackBar(text)
//            stopAnimating()
//        case .profile:
//            populateDetail()
//        default:
//            break
//        }
//    }
//}
