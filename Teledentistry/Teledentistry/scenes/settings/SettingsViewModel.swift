//
//  SettingsViewModel.swift
//  Clustry
//
//  Created by cis on 05/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

final class SettingsViewModel {

    private var items: [RowSectionDisplayable] = []
    var didSelect:((_ item:RowSectionDisplayable) -> Void)?
//    var user: User?
    var view: ProfileViewRepresentable?
    let provider: ProfileServiceProvidable
    
    var count:Int {
        return items.count
    }
    
    init(provider: ProfileServiceProvidable) {
        self.provider = provider
        self.provider.delegate = self
        items = RowSectionDisplayableFunction.makeRowSectionDisplayable(resource: "Settings")
    }
    
    func profile() {
//        provider.profile(param: UserParams.Profile(id: UserStore.userID))
    }
    
    func feedback() {
    
    }

    
    func items(in section: Int) -> [Row] {
        return items[section].content
    }
    
    func item(at section: Int) -> RowSectionDisplayable {
        return items[section]
    }
    
    func item(for indexPath: IndexPath) -> Row {
        return items(in: indexPath.section)[indexPath.row]
    }
}

extension SettingsViewModel: ProfileServiceProviderDelegate {
    func completed<T>(for action: ProfileAction, with response: T?, error: APIError?) {
        DispatchQueue.main.async {
            if error != nil {
                self.view?.onAction(.errorMessage(errorMessage))
            } else {
//                self.user = (response as? SuccessResponseModel)?.data?.user
                self.view?.onAction(action)
            }
        }
    }
}

