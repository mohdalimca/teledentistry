//
//  InputFieldAlert.swift
//  Clustry
//
//  Created by cis on 18/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

enum InputFieldAlertType:String {
    case email
    case password
    case title
    case days
    case goal
    case objective
    case maplink
    case weblink
    case points
    case designation
}

class InputFieldAlert: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFieldTitle: UILabel!
    @IBOutlet weak var btnEnter: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var viewInput: UIView!
    @IBOutlet weak var txtInputField: UITextField!
    
    var isEmailAddress: Bool!
    var alertType:InputFieldAlertType!
    weak var delegate:InputFieldAlertDelegate?
    let onboarding = OnboardingViewModel()
    var editValue:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
    }
    
    private func setup() {
        txtInputField.delegate = self
        viewInput.borderColor = .lightGray
        txtInputField.addTarget(self, action: #selector(textFieldDidChanged(textField:)), for: .editingChanged)
        btnEye.isHidden = true
        [btnEnter, btnCancel, btnEye].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        
        switch alertType {
        case .email:
            setValue(mainTitle: "Email Address", labelTitle: "Email Address", placeholder: "Enter email", keyBoardType: .emailAddress)
        case .password:
            btnEye.isHidden = false
            txtInputField.isSecureTextEntry = true
            if UserStore.userPassword != nil {
                setValue(mainTitle: "Password Protected", labelTitle: "Password", placeholder: "Enter Password", keyBoardType: .default)
            } else {
                setValue(mainTitle: "Set password to Protect your valuable docs", labelTitle: "Password", placeholder: "Enter Password", keyBoardType: .default)
            }
            
        case .title:
            setValue(mainTitle: "Enter Title", labelTitle: "Title", placeholder: "Enter Title", keyBoardType: .default)
        case .days:
            setValue(mainTitle: "Enter number of Days", labelTitle: "Days", placeholder: "Enter Days", keyBoardType: .numberPad)
        case .goal:
            setValue(mainTitle: "Enter the Goal", labelTitle: "Goal", placeholder: "Enter goal...", keyBoardType: .default)
        case .objective:
            setValue(mainTitle: "Enter the Objective", labelTitle: "Objective", placeholder: "Enter objective...", keyBoardType: .default)
        case .maplink:
            setValue(mainTitle: "Enter Map Link", labelTitle: "Map Link", placeholder: "Enter map link", keyBoardType: .default)
        case .weblink:
            setValue(mainTitle: "Enter Web Link", labelTitle: "Web Link", placeholder: "Enter web link", keyBoardType: .default)
        case .points:
            setValue(mainTitle: "Enter Points", labelTitle: "Points", placeholder: "Enter points", keyBoardType: .numberPad)
        case .designation:
            setValue(mainTitle: "Enter Designation", labelTitle: "Designation", placeholder: "Enter designation", keyBoardType: .default)
        default:
            break
        }
        
        if editValue != "" {
            txtInputField.text = editValue
        }
    }
    
    private func setValue(mainTitle:String, labelTitle:String, placeholder:String, keyBoardType:UIKeyboardType) {
        lblTitle.text = mainTitle
        lblFieldTitle.text = labelTitle
        txtInputField.placeholder = placeholder
        txtInputField.keyboardType = keyBoardType
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        if sender == btnEnter {
            if txtInputField.text?.trimmingCharacters(in: .whitespaces).count != 0 {
                switch alertType {
                case .email:
                    guard onboarding.validateEmail(email: txtInputField.text!) else {
                        showSnackBar("Invalid email address")
                        return
                    }
                case .password:
                    if UserStore.userPassword != nil && UserStore.userPassword != txtInputField.text?.trimmingCharacters(in: .whitespaces) {
                        showSnackBar("Please enter correct password")
                        return
                    } else {
                        UserStore.save(userPassword: txtInputField.text!.trimmingCharacters(in: .whitespaces))
                    }
                default:
                    break
                }
                delegate?.userInput(txtInputField.text!)
                dismiss(animated: true, completion: nil)
                
            } else {
                showSnackBar("Plase enter your input")
            }
        } else if sender == btnEye {
            txtInputField.isSecureTextEntry = !txtInputField.isSecureTextEntry
        }
    }
}

extension InputFieldAlert: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        viewInput.borderColor = .orange
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewInput.borderColor = .lightGray
    }
    
    @objc func textFieldDidChanged(textField: UITextField) {
        txtInputField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch alertType {
        case .email:
            if let _ = string.rangeOfCharacter(from: .whitespacesAndNewlines) {
                return false
            }
            return true
        default:
            return true
        }
    }
}

