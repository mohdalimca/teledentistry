//
//  CustomPopup.swift
//  Clustry
//
//  Created by cis on 18/11/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

import Foundation
import PopupDialog

class CustomPopup {
    static let shared = CustomPopup()
    
    func showPopup(controller: UIViewController, buttons: [PopupDialogButton]) -> PopupDialog {
        let popup: PopupDialog = PopupDialog(viewController: controller, buttonAlignment: .horizontal, transitionStyle: .bounceUp, tapGestureDismissal: true)
//        _ = PopupDialogContainerView.appearance()
//        containerAppearance.layer.cornerRadius = 20
//        popup.addButtons(buttons)
        return popup
    }
    
}
