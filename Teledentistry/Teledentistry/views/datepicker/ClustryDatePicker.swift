//
//  ClustryDatePicker.swift
//  Clustry
//
//  Created by cis on 05/12/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

class ClustryDatePicker: UIView {
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var containerView : UIView!
    @IBOutlet var btnCancel : UIButton!
    
    static let shared = ClustryDatePicker()
    
    var selectedDate : Date?
    var selectedDateCallback : ((Date) -> Void)?
    var mode : UIDatePicker.Mode = .dateAndTime
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        Bundle.main.loadNibNamed("ClustryDatePicker", owner: self, options: nil)
        addSubview(containerView)
        containerView.bounds = UIScreen.main.bounds
        containerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        containerView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
    }
    
    func present(){
        datePicker.datePickerMode = mode
        UIApplication.shared.keyWindow?.addSubview(containerView)
    }
    
    func dismiss(){
        containerView.removeFromSuperview()
    }
    
    @IBAction func datePickerChangedValue(_ sender: Any) {
        if let datePicker = sender as? UIDatePicker{
            //            print(Util.UTCToLocal(date: "\(datePicker.date)", inFormat: "yyyy-MM-dd HH:mm:ss +zzzz", outFormat: "hh:mm a"))
            selectedDate = datePicker.date
        }
    }
    
    @IBAction func btnOkPressed(_ sender : UIButton){
        selectedDateCallback?(selectedDate ?? Date())
        dismiss()
    }
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        dismiss()
    }
}
