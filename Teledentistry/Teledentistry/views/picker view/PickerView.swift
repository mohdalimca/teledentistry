//
//  PickerView.swift
//  Clustry
//
//  Created by cis on 05/12/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import UIKit

protocol GetPickerValueDelagate {
    func getValue(_ value:String, index:Int, scene:Scenes?)
}

class PickerView: UIViewController {
    
    var delegate:GetPickerValueDelagate?
    var index = 0
    var selectedValue = ""
    var data = [String]()
    var isType: Bool!
    var scene: Scenes?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if data.count > 0 {
            self.selectedValue = data[self.index]
        }
    }
    
    @IBAction func btnCancelAction(_ sender: UIBarButtonItem) {
        self.delegate?.getValue("", index: -1, scene: scene)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneAction(_ sender: UIBarButtonItem) {
        self.delegate?.getValue(self.selectedValue, index: self.index, scene: scene)
        self.dismiss(animated: true, completion: nil)
    }
}

extension PickerView:UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if data.count > 0 {
            self.index = row
            self.selectedValue = data[row]
        }
    }
}

